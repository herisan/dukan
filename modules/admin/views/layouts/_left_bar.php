<?php

use yii\helpers\Url;
use app\helpers\LinkHelper;
$session = Yii::$app->session;
?>
<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= Url::base()?>/vendor/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= ($session->get('username')) ? $session->get('username') : '["Username"]' ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li <?= LinkHelper::left_bar_active(Url::to(['default/index']))?>>
          <a href="<?= Url::to(['default/index'])?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-spoon"></i>
            <span>Food</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?= LinkHelper::left_bar_active(Url::to(['food/index']))?>><a href="<?= Url::to(['food/index'])?>"><i class="fa fa-circle-o"></i> List Food</a></li>
            <li <?= LinkHelper::left_bar_active(Url::to(['food/food-cat']))?>><a href="<?= Url::to(['food/food-cat'])?>"><i class="fa fa-circle-o"></i> Categories</a></li>
            <li <?= LinkHelper::left_bar_active(Url::to(['food/food-type']))?>><a href="<?= Url::to(['food/food-type'])?>"><i class="fa fa-circle-o"></i> Types</a></li>
          </ul>
        </li>
        <li <?= LinkHelper::left_bar_active(Url::to(['testimoni/index']))?>>
          <a href="<?= Url::to(['testimoni/index'])?>">
            <i class="fa fa-comment"></i> <span>Testimoni</span>
          </a>
        </li>
        <li <?= LinkHelper::left_bar_active(Url::to(['account/index']))?>>
          <a href="<?= Url::to(['account/index'])?>">
            <i class="fa fa-user"></i> <span>Account</span>
          </a>
        </li>
        <li <?= LinkHelper::left_bar_active(Url::to(['default/language']))?>>
          <a href="<?= Url::to(['default/language'])?>">
            <i class="fa fa-language"></i> <span>Language</span>
          </a>
        </li>
        <!-- <li >
          <a href="<?= Url::to(['default/sad'])?>">
            <i class="fa fa-language"></i> <span>sad</span>
          </a>
        </li> -->
        <li class="header">OTHER</li>
        <li class="">
          <a href="<?= Url::home()?>" target="_blank">
            <i class="fa fa-desktop"></i> <span>Application</span>
          </a>
        </li>
      </ul>
    </section>