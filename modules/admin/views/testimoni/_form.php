<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\helpers\ModuleHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Testimoni */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $this->registerCssFile("@web/vender/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")?>

<div class="testimoni-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'method')->dropdownList(ModuleHelper::getMethodList(),['prompt'=>'Select Method']) ?>

    <?= $form->field($model, 'phase')->dropdownList(ModuleHelper::getPhaseList(),['prompt'=>'Select Phase']) ?>

    <?= $form->field($upload, 'imageFile')->fileInput(['accept' => 'image/jpg,image/png','class'=>'form-control']) ?>

    <?= $form->field($model, 'desc')->textarea(['rows' => 6,'class'=>'textarea form-control','Place some text here']) ?>

    <?= $form->field($model, 'age')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'wstart')->textInput(['maxlength' => true,'type' => 'number'])->label('First Weight (kg)') ?>

    <?= $form->field($model, 'wlast')->textInput(['maxlength' => true,'type' => 'number'])->label('Last Weight (kg)') ?>

    <div class="form-group">
        <?= Html::a('Cancel',['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJsFile("@web/vender/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")?>
<script>
  $(function () {
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>