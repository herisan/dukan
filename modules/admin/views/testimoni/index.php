<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

use app\models\Food;
$session = Yii::$app->session;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Testimoni';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $this->title;?>
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Url::to(['default/index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?= $this->title;?></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="testimoni-index">

                <p>
                    <?= Html::a('Create Testimoni', ['create'], ['class' => 'btn btn-success']) ?>
                </p>


                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'idtestimoni',
                        [
                            'attribute'=>'method',
                            'value'=>function($model){
                                return Food::getMethod($model->method);
                            }
                        ],
                        [
                            'attribute'=>'phase',
                            'value'=>function($model){
                                return Food::getPhase($model->phase);
                            }
                        ],
                        [ 
                            'attribute'=>'image',
                            'format'=>'html',
                            'value'=>function($model){
                                return Html::img('@web/img/testimoni/'.$model->image, ['alt' => 'My logo','style'=>'width:125px']);
                            }
                        ],
                        'desc:html',
                        //'lang',
                        //'age',
                        //'dateinput',

                        ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}','buttons'=>[
                                  'update' => function ($url, $model) {
                      $url = Url::to(['update','id'=>$model->idtestimoni]); 
                  return Html::a('<span class="badge bg-blue"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                    'title' => Yii::t('yii', 'Update'),
                  ]);                                
                                  },
                                  'delete' => function ($url, $model) { 
                      $url = Url::to(['delete','id'=>$model->idtestimoni]); 
                  return Html::a('<span class="badge bg-red"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                    'title' => Yii::t('yii', 'Delete'),
                    'onclick'=>'return confirm(\'Are you sure you want to delete this data?\')'
                  ]);                                
                                  },
                              ]],
                    ],
                ]); ?>


            </div>
        </div>
    </div>
</div>
