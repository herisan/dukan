<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Testimoni */

$this->title = 'Update Testimoni: ';
$this->params['breadcrumbs'][] = ['label' => 'Testimonis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idtestimoni, 'url' => ['view', 'id' => $model->idtestimoni]];
$this->params['breadcrumbs'][] = 'Update';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $this->title;?>
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Url::to(['default/index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?= Url::to(['testimoni/index'])?>">Testimoni</a></li>
    <li class="active"><?= $this->title;?></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">

<div class="row">
    <div class="col-md-12">
        <div class="box">
			<div class="testimoni-update">

			    <?= $this->render('_form', [
			        'model' => $model,
			        'upload' => $upload,
			    ]) ?>

			</div>
		</div>
	</div>
</div>
