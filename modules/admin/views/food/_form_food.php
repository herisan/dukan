<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\helpers\ModuleHelper;
$this->title = $title;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $this->title;?>
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Url::to(['default/index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?= Url::to(['food/index'])?>">Food</a></li>
    <li class="active"><?= $this->title;?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
              <div class="box-body">
                <div class="col-md-6">
                <?= $form->field($model, 'idfoodtype')->dropdownList(ArrayHelper::map($type,'idfoodtype','name'),
                      ['prompt'=>'Select Type']) ?>
                <?= $form->field($model, 'idfoodcat')->dropdownList(ArrayHelper::map($cat,'idfoodcat','name'),
                      ['prompt'=>'Select Category']) ?>
                <?= $form->field($model, 'method')->dropdownList(ModuleHelper::getMethodList(),
                      ['prompt'=>'Select Method']
                  ) ?>
                <?= $form->field($model, 'phase')->dropdownList(ModuleHelper::getPhaseList(),
                      ['prompt'=>'Select Phase']
                  ) ?>
                <?= $form->field($model, 'idfood') ?>
                <?= $form->field($model, 'name') ?>
                </div>
                <div class="col-md-6">
                <?= $form->field($upload, 'imageFile')->fileInput(['accept' => 'image/jpg,image/png','class'=>'form-control']) ?>
                <?= $form->field($model, 'desc')->textArea() ?>
                <?= $form->field($model, 'lang')->dropdownList([
                          'fr' => 'French', 
                          'en' => 'English',
                          'es' => 'Spanish',
                          'it' => 'Italian',
                          'tr' => 'Turkish',
                          'pr' => 'Portuguese',
                          'ru' => 'Russian',
                          'cn' => 'Chinese',
                      ],
                      ['prompt'=>'Select Language','onchange'=>'$("input#food-country").val($(this).val().toUpperCase())']
                  ) ?>
                <?= $form->field($model, 'country')->hiddenInput()->label(false) ?>
                </div>
              </div>
              <div class="box-footer">
                <a href="<?= Url::to(['food/index'])?>">
                <?= Html::a(Html::button('Cancel',['class'=>'btn btn-default']),['food/index'])?>
                <?= Html::submitButton('Save',['class'=>'btn btn-primary'])?>
              </div>
            <?php ActiveForm::end() ?>
          <!-- /.box -->
        </div>
      </div>
</section>
<!-- /.content -->