<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Update Food Category";
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $this->title;?>
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Url::to([''])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?= Url::to(['food/food-cat'])?>">Food Categories</a></li>
    <li class="active"><?= $this->title;?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Update Food Category</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php $form = ActiveForm::begin([]) ?>
                <div class="box-body">
                  <?= $form->field($model, 'idfoodcat')->textInput(['readonly'=>true]) ?>
                  <?= $form->field($model, 'name') ?>
                  <?= $form->field($model, 'lang')->hiddenInput()->label(false) ?>
                </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <?= Html::a(Html::button('Cancel',['class'=>'btn btn-default']),['food/food-cat'])?>
                <?= Html::submitButton('Submit',['class'=>'btn btn-primary'])?>
              </div>
            <?php ActiveForm::end() ?>
          </div>
          <!-- /.box -->
        </div>
      </div>

</section>
<!-- /.content -->