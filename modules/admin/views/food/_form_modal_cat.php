<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$session = Yii::$app->session;
?>
<div id="modal-add" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modal Header</h4>
		</div>
	    <?php $form = ActiveForm::begin([]) ?>
		<div class="modal-body">
	      <?= $form->field($model, 'idfoodcat') ?>
	      <?= $form->field($model, 'name') ?>
	      <?= $form->field($model, 'lang')->hiddenInput(['value'=>$session->get('language')])->label(false) ?>
	      <?= Html::button('Cancel',['class'=>'btn btn-default','data-dismiss'=>'modal'])?>
	      <?= Html::submitButton('Submit',['class'=>'btn btn-primary'])?>
    	</div>
      	<div class="modal-footer">
		      <?= Html::button('Cancel',['class'=>'btn btn-default','data-dismiss'=>'modal'])?>
		      <?= Html::submitButton('Submit',['class'=>'btn btn-primary'])?>
      	</div>
	    <?php ActiveForm::end() ?>
    </div>

  </div>
</div>