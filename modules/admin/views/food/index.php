<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

use app\models\Food;

$this->title = "List Food";
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $this->title;?>
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Url::to(['default/index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?= $this->title;?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

<div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <?php $form = ActiveForm::begin(['id'=>'form-search','method'=>'get','options' => ['enctype' => 'multipart/form-data']]) ?>
            <div class="col-md-6">
              <?= $form->field($food, 'idfoodtype')->dropdownList(ArrayHelper::map($type,'idfoodtype','name'),
                    ['prompt'=>'Select Type','onchange'=>'$("#form-search").submit()']) ?>
            </div>
            <div class="col-md-6">
              <?= $form->field($food, 'idfoodcat')->dropdownList(ArrayHelper::map($cat,'idfoodcat','name'),
                    ['prompt'=>'Select Category','onchange'=>'$("#form-search").submit()']) ?>
            </div>
          <?php ActiveForm::end() ?>
          <div class="col-md-12">
            <?= Html::a(Html::button('Add',['class'=>'btn btn-primary']),['food/food-add'])?>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="overflow-x: auto;">
          <?= GridView::widget([
              'dataProvider' => $dataProvider,
              'columns' => [
                  ['class' => 'yii\grid\SerialColumn'],
                  'name',
                  [
                    'attribute'=>'method',
                    'value'=> function($model){
                      return Food::getMethod($model->method);
                    },
                  ],
                  [
                    'attribute'=>'phase',
                    'value'=> function($model){
                      return Food::getPhase($model->phase);
                    },
                  ],
                  [
                    'attribute'=>'image',
                    'format'=>'html',
                    'value'=> function($model){
                      return Html::img('@web/img/admin/food/'.$model->image, ['alt' => 'My logo','style'=>'width:125px']);
                    },
                  ],
                  'desc',
                  ['class' => 'yii\grid\ActionColumn',
                    'template'=>'{update} {delete}',
                    'buttons'=>[
                                  'update' => function ($url, $model) {
                      $url = Url::to(['food/food-update','id'=>$model->idfood]); 
                  return Html::a('<span class="badge bg-blue"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                    'title' => Yii::t('yii', 'Update'),
                  ]);                                
                                  },
                                  'delete' => function ($url, $model) { 
                      $url = Url::to(['food/food-delete','id'=>$model->idfood]); 
                  return Html::a('<span class="badge bg-red"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                    'title' => Yii::t('yii', 'Delete'),
                    'onclick'=>'return confirm(\'Are you sure you want to delete this data?\')'
                  ]);                                
                                  },
                              ]
                  ],
                ]
          ]);?>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
  </div>
</div>

</section>
<!-- /.content -->