<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Update Food Type";
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $this->title;?>
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Url::to(['default/index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?= Url::to(['food/food-type'])?>">Food Type</a></li>
    <li class="active"><?= $this->title;?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- form start -->
            <?php $form = ActiveForm::begin([]) ?>
                <div class="box-body">
                  <?= $form->field($model, 'idfoodtype')->textInput(['readonly'=>true]) ?>
                  <?= $form->field($model, 'name') ?>
                  <?= $form->field($model, 'lang')->hiddenInput()->label(false) ?>
                </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <?= Html::a(Html::button('Cancel',['class'=>'btn btn-default']),['food/food-type'])?>
                <?= Html::submitButton('Submit',['class'=>'btn btn-primary'])?>
              </div>
            <?php ActiveForm::end() ?>
          </div>
          <!-- /.box -->
        </div>
      </div>

</section>
<!-- /.content -->