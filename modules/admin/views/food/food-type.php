<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = "Food Type";
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $this->title;?>
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Url::to([''])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?= $this->title;?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="v2-food-type">
		<div class="row">
	        <div class="col-md-12">
	          <div class="box">
        		<div class="box-header">
	            	<?= Html::button('Add',['class'=>'btn btn-primary','data-toggle'=>'modal','data-target'=>'#modal-add'])?>
        		</div>
	            <!-- /.box-header -->
	            <div class="box-body">
	            	<?= GridView::widget([
					    'dataProvider' => $dataProvider,
					    'columns' => [
        					['class' => 'yii\grid\SerialColumn'],
        					'name',
        					['class' => 'yii\grid\ActionColumn',
        						'template'=>'{update}{delete}',
        						'buttons'=>[
                              		'update' => function ($url, $model) {
        							$url = Url::to(['food/food-type-update','id'=>$model->idfoodtype]);	
									return Html::a('<span class="badge bg-blue"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
									]);                                
                              		},
                              		'delete' => function ($url, $model) {	
        							$url = Url::to(['food/food-type-delete','id'=>$model->idfoodtype]);	
									return Html::a('<span class="badge bg-red"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
										'title' => Yii::t('yii', 'Delete'),
										'onclick'=>'return confirm(\'Are you sure you want to delete this data?\')'
									]);                                
                              		},
                          		]
                      		],
        				]
					]);?>
	            </div>
	            <!-- /.box-body -->
	          </div>
	          <!-- /.box -->
	      </div>
	  </div>

	  <?= $this->render('_form_modal_type',['model'=>$model]);?>
	</div>

</section>
<!-- /.content -->