<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
$this->title = $title;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $this->title;?>
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Url::to(['default/index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?= Url::to(['account/index'])?>">Account</a></li>
    <li class="active"><?= $this->title;?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
              <div class="box-body">
              	<?= $form->field($profile, 'fullname') ?>
                <?= $form->field($model, 'email')->input('email') ?>
              	<?= $form->field($model, 'password')->passwordInput(['value'=>'']) ?>
              	<?= $form->field($model, 'passwordrepeat')->passwordInput(['value'=>''])->label('Password Repeat') ?>
              	<?= $form->field($model, 'role')->dropdownList([
                          '1' => 'ADMIN', 
                          '2' => 'USER',
                      ],
                      ['prompt'=>'Select Role']
                  ) ?>
              	<?= $form->field($model, 'status')->dropdownList([
                          '0' => 'NOT ACTIVE', 
                          '1' => 'ACTIVE',
                      ],
                      ['prompt'=>'Select Status','disabled'=>($model->idaccount==Yii::$app->user->id) ? true : false]
                  ) ?>
              </div>
              <div class="box-footer">
                <?= Html::a(Html::button('Cancel',['class'=>'btn btn-default']),['account/index'])?>
                <?= Html::submitButton('Save',['class'=>'btn btn-primary'])?>
              </div>
            <?php ActiveForm::end() ?>
          <!-- /.box -->
        </div>
      </div>
</section>
<!-- /.content -->