<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Profile";
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $this->title;?>
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Url::to(['default/index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?= $this->title;?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

<div class="row">
	<div class="col-md-12">
      		<?php $form = ActiveForm::begin(['id'=>'form-profile','method'=>'post','options' => ['enctype' => 'multipart/form-data']]) ?>
      <div class="box">
      	<div class="box-body">
      		<div class="col-md-6">
	      		<?= $form->field($profile, 'fullname') ?>
	      		<?= $form->field($profile, 'name') ?>
	      		<?= $form->field($profile, 'gender')->dropdownList([
                          'M' => 'Male', 
                          'F' => 'Female',
                      ],
                      ['prompt'=>'Select Gender']
                  ) ?>
	      		<?= $form->field($profile, 'phone') ?>
	      		<?= $form->field($profile, 'postalcode') ?>
	      		<?= $form->field($profile, 'address')->textArea() ?>
	      		<?= $form->field($profile, 'city') ?>
      		</div>
      		<div class="col-md-6">
      			<?= $form->field($profile, 'country') ?>
	      		<?= $form->field($profile, 'dob')->widget(\yii\jui\DatePicker::class, [
					    'dateFormat' => 'MM/dd/yyyy',
					    'options'=>['class'=>'form-control'],
					    'clientOptions' => ['defaultDate' => date('d-m-Y')]
						])->label('Date of Birth') ?>
	      		<?= $form->field($profile, 'motivation')->textArea() ?>
	      		<?= $form->field($profile, 'relationship') ?>
	      		<?= $form->field($profile, 'children') ?>
	      		<?= $form->field($upload, 'imageFile')->fileInput(['accept' => 'image/jpg,image/png','class'=>'form-control']) ?>

              	<?php
              	/*if (file_exists(Yii::$app->basePath . '/web/img/v2/profile/'.$profile->image)) {
              		echo Html::img('@web/img/v2/profile/'.$profile->image, ['alt' => 'My logo','style'=>'width:125px']);
              	}*/
              	?>
      		</div>
      	</div>
      	<div class="box-footer">
      		<div class="col-md-12">
      		<div class="form-group">
      			<?= Html::button('Submit',['type'=>'submit','class'=>'btn btn-primary'])?>
      		</div>
      		</div>
      	</div>
      </div>
          <?php ActiveForm::end() ?>
      	<?= $alert;?>
  	</div>
</div>
</section>