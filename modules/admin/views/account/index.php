<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = "Account";
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $this->title;?>
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Url::to(['default/index'])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?= $this->title;?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

<div class="row">
	<div class="col-md-12">
    <div class="box">
      <div class="box-header">
        <?= Html::a(Html::button('Add',['class'=>'btn btn-primary']),['account/add'])?>
      </div>
      <div class="box-body">
        <?= GridView::widget([
          'dataProvider' => $dataProvider,
          'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'email',
            [
              'attribute'=>'role',
              'value'=> function($model){
                return ($model->role==1) ? 'Admin' : 'User';
              },
            ],
            'lastlogin',
            [
              'attribute'=>'status','format'=>'raw',
              'value'=> function($model){
                $disabled = ($model->idaccount==Yii::$app->user->id) ? true : false;
                return Html::checkbox($model->status,($model->status==1) ? true : false,['data-toggle'=>'toggle','data-on'=>'Active','data-off'=>'Not Active','data-onstyle'=>'success','data-offstyle'=>'danger','onchange'=>'change_status('.$model->idaccount.',this.checked)',' data-size'=>'mini','disabled'=>$disabled]);
              },
            ],
            [ 
              'class' => 'yii\grid\ActionColumn','template'=>'{update}{delete}',
              'buttons'=>[
                'update' => function ($url, $model) {
                $url = Url::to(['account/update','id'=>$model->idaccount]); 
                return Html::a('<span class="badge bg-blue"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                  'title' => Yii::t('yii', 'Update'),
                  ]);
                },
                'delete' => function ($url, $model) { 
                $pointer = ($model->idaccount==Yii::$app->user->id) ? 'pointer-events:none' : '';//if current user account
                $url = Url::to(['account/delete','id'=>$model->idaccount]); 
                return Html::a('<span class="badge bg-red"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                  'title' => Yii::t('yii', 'Delete'),
                  'onclick'=>'return confirm(\'Are you sure you want to delete this data?\')','style'=>$pointer,
                  ]);                                
                },
              ]
            ],
          ]
        ]);?>
      </div>
      <!-- /.box-body -->
    </div>
	</div>
</div>
</section>
<script type="text/javascript">
  function change_status(id,status){
    $.ajax({
       url : "<?= Url::to(['account/change-status']);?>",
       type : "GET",
       data : "id="+id+"&status="+status,
       success : function(data){
          
       }
     });
  }
</script>