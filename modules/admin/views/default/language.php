<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = "Language";
$session = Yii::$app->session;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $this->title;?>
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Url::to([''])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?= $this->title;?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
  <!-- left column -->
  <div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Change Language</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <?php $form = ActiveForm::begin(['id'=>'form-lang','options'=>['role'=>'form']]) ?>
        <div class="box-body">
          <?= $form->field($language, 'lang', [
              'options' => ['class' => 'form-group'],
          ])->radioList([
                'fr' => Html::img('@web/img/lang/lang_fr.png',['style'=>'width:25px']).' French', 
                'en' => Html::img('@web/img/lang/lang_en.png',['style'=>'width:25px']).' English',
                'es' => Html::img('@web/img/lang/lang_es.png',['style'=>'width:25px']).' Spanish',
                'it' => Html::img('@web/img/lang/lang_it.png',['style'=>'width:25px']).' Italian',
                'tr' => Html::img('@web/img/lang/lang_tr.png',['style'=>'width:25px']).' Turkish',
                'pr' => Html::img('@web/img/lang/lang_pr.png',['style'=>'width:25px']).' Portuguese',
                'ru' => Html::img('@web/img/lang/lang_ru.png',['style'=>'width:25px']).' Russian',
                'de' => Html::img('@web/img/lang/lang_de.png',['style'=>'width:25px']).' Germany',
                'cn' => Html::img('@web/img/lang/lang_cn.png',['style'=>'width:25px']).' Chinese',
            ],['value'=>$session->get('language'),'encode'=>false,'class'=>'radio'])->label(false)?>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <?= Html::submitButton('Save Changes',['class'=>'btn btn-primary'])?>
        </div>
      <?php ActiveForm::end() ?>
    </div>
    <!-- /.box -->
    <div id="alerts">
    <?= $alert;?></div>
  </div>
</div>

</section>
<!-- /.content -->