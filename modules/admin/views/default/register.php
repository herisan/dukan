<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = "Register";
?>

<div class="login-box">
  <div class="login-logo">
    <b>Admin Panel</b>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Register a new membership</p>

      <?php $form = ActiveForm::begin([
        'id' => 'register-form',
    ]); ?>
      <?= $form->field($profile, 'fullname', [
              'options' => ['class' => 'form-group has-feedback'],
              'template' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
          ])->textInput(['autofocus' => true,'placeholder'=>'Fullname'])->label(false) ?>
      <?= $form->field($profile, 'name', [
              'options' => ['class' => 'form-group has-feedback'],
              'template' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
          ])->textInput(['placeholder'=>'Name'])->label(false) ?>
      <?= $form->field($account, 'email', [
              'options' => ['class' => 'form-group has-feedback'],
              'template' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
          ])->textInput(['placeholder'=>'Email'])->label(false) ?>
      <?= $form->field($account, 'password', [
              'options' => ['class' => 'form-group has-feedback'],
              'template' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
          ])->passwordInput(['placeholder'=>'Password'])->label(false) ?>
      <?= $form->field($account, 'passwordrepeat', [
              'options' => ['class' => 'form-group has-feedback'],
              'template' => "{input}<span class='glyphicon glyphicon-log-in form-control-feedback'></span>"
          ])->passwordInput(['placeholder'=>'Password Repeat'])->label(false) ?>
      <?= $form->field($profile, 'gender', [
              'options' => ['class' => 'form-group has-feedback'],
              'template' => "{input}",
          ])->radioList([
              'M' => 'Male', 
              'F' => 'Female'
          ])->label(false) ?>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <?= Html::submitButton('Register',['class'=>'btn btn-primary btn-block btn-flat','disabled'])?>
        </div>
        <!-- /.col -->
      </div>
    <?php ActiveForm::end(); ?>
    <?= Html::a('I already have a membership',['default/login'],['class'=>'text-center'])?>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->