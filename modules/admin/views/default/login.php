<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = "Login";
?>

<div class="login-box">
  <div class="login-logo">
    <b>Admin Panel</b>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

      <?php $form = ActiveForm::begin([
        'id' => 'login-form',
      ]); ?>
        <?= $form->field($model, 'username', [
              'options' => ['class' => 'form-group has-feedback'],
              'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
          ])->textInput(['autofocus' => true,'placeholder'=>'Email'])->label(false) ?>
        <?= $form->field($model, 'password', [
              'options' => ['class' => 'form-group has-feedback'],
              'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
          ])->passwordInput(['placeholder'=>'Password'])->label(false) ?>
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
              <label>
            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => "{input} {label}\n<div class=\"col-lg-12\">{error}</div>",
            ]) ?>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <?= Html::submitButton('Sign In',['class'=>'btn btn-primary btn-block btn-flat'])?>
          </div>
        <!-- /.col -->
        </div>
    <?php ActiveForm::end(); ?>
    <?= Html::a('I forgot my password',['#'])?><br>
    <?= Html::a('Register a new membership',['default/register'],['class'=>'text-center'])?>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->