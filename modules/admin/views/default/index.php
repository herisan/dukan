<?php
use yii\helpers\Url;

$this->title = "Dashboard";
$session = Yii::$app->session;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= $this->title;?>
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= Url::to([''])?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?= $this->title;?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="v2-default-index">
    <h2>Hai, <?= $name;?></h2>
    <h1>Welcome back to Admin Panel</h1>
</div>

</section>
<!-- /.content -->