<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Account;
use app\models\User;
use app\models\Profile;
use app\modules\admin\models\UploadImage;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

class AccountController extends \yii\web\Controller
{
	public function init() 
	{
	    parent::init();
	    $this->layout = '@app/modules/admin/views/layouts/main';
	    // custom initialization code goes here
	}

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['default/login']);
        }
        return parent::beforeAction($action);
    }
    public function actionIndex()
    {
        $pagesize=10;
        $role = Account::findOne(Yii::$app->user->id);
        if ($role->role!=1) {
            return $this->redirect(['default/error']);
        }
        $query = Account::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pagesize,
            ],
        ]);
        return $this->render('index',['dataProvider'=>$dataProvider]);
    }
    public function actionAdd()
    {
    	$model = new Account();
        $profile = new Profile();
    	if ($model->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
    		if($model->save()){
                $account = Account::findOne(['email'=>$model->email]);
                $account->password = md5($account->password);
                $account->passwordrepeat = md5($account->passwordrepeat);
                $account->save();
                $profile->name = $profile->fullname;
                $profile->gender = 'M';
                $profile->idaccount = $model->idaccount;
                if($profile->save()){
                    return $this->redirect(['index']);
                }
    		}
    	}
    	return $this->render('_form',['model'=>$model,'profile'=>$profile,'title'=>'Add Account']);
    }
    public function actionUpdate($id)
    {
        $model = Account::findOne($id);
        $profile = Profile::findOne(['idaccount'=>$id]);
        if ($model->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            
            if($model->save()){
                $account = Account::findOne(['email'=>$model->email]);
                $account->password = md5($model->password);
                $account->passwordrepeat = md5($model->passwordrepeat);
                $account->save();
                $profile->name = $profile->fullname;
                $profile->gender = 'M';
                $profile->idaccount = $model->idaccount;
                if($profile->save()){
                    return $this->redirect(['index']);
                }
            }
        }
        return $this->render('_form',['model'=>$model,'profile'=>$profile,'title'=>'Update Account']);
    }
    public function actionDelete($id)
    {
        $profile = Profile::findOne(['idaccount'=>$id]);
        if (!empty($profile)) {
            if(!empty($profile->image))
            {
                UploadImage::unLinkImage(Yii::$app->basePath . '/web/img/admin/profile/' . $profile->image);
            }
            $profile->delete();
        }
        $model = Account::findOne($id);
        $model->delete(); 

    	return $this->redirect(['index']);
    }

    public function actionProfile()
    {
        $alert = '';
        $date = date('YmdHis');
        $profile = Profile::findOne(['idaccount'=>Yii::$app->user->id]);
        $upload = new UploadImage();
        if ($profile->load(Yii::$app->request->post())) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            //check if file not empty
            if(!empty($upload->imageFile) && $upload->imageFile->size !== 0) {
                //rename filename
                $filename = $upload->removeSpecialChar($upload->imageFile->baseName);
                //upload image
                $profileimage = $upload->uploadImg($profile->image,$date,$filename,'img/admin/profile/');
                if ($profileimage==true) {
                    //rename image
                    $profile->image = $filename . $date . '.' . $upload->imageFile->extension;
                }
            }else{
                //no rename image
                $profile->image = $profile->image;
            }

            $datedob = date_create($profile->dob);
            $profile->dob = date_format($datedob,'Y-m-d');
            if($profile->save()){
                $alert = '<div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    Success to change profile.
                  </div>';
            }
        }

        return $this->render('profile',['profile'=>$profile,'upload'=>$upload,'alert'=>$alert]);
    }

    public function actionChangeStatus($id,$status)
    {
        $status = ($status=='true') ? 1 : 0;
        $command = Yii::$app->db->createCommand()
            ->update('account',['status'=>$status],['idaccount'=>$id]);
        $command->execute();
    }

}
