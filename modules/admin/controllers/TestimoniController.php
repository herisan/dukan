<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Testimoni;
use app\modules\admin\models\UploadImage;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

/**
 * TestimoniController implements the CRUD actions for Testimoni model.
 */
class TestimoniController extends Controller
{
    private function Session(){
        return Yii::$app->session; // start session
    }
    private function Pagesize(){
        return 10; //page size 10
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['default/login']);
        }
        return parent::beforeAction($action);
    }
    /**
     * Lists all Testimoni models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Testimoni::find()->where(['lang'=>$this->session()->get('language')]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
             'pageSize' => $this->pagesize(),
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Testimoni model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        // return $this->render('view', [
        //     'model' => $this->findModel($id),
        // ]);
        return $this->redirect(['index']);
    }

    /**
     * Creates a new Testimoni model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $date = date('YmdHis');
        $model = new Testimoni();
        $upload = new UploadImage();

        if ($model->load(Yii::$app->request->post())) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            //check if file not empty
            if(!empty($upload->imageFile) && $upload->imageFile->size !== 0) {
                //rename filename
                $filename = $upload->removeSpecialChar($upload->imageFile->baseName);
                //upload image
                $modelimage = $upload->uploadImg($model->image,$date,$filename,'img/testimoni/');
                if ($modelimage==true) {
                    //save image
                    $model->image = $filename . $date . '.' . $upload->imageFile->extension;
                }
            }else{
                $model->image = "-";
            }
            
            $model->lang = $this->session()->get('language');
            $model->dateinput = $date;
            $model->save();
            // return $this->redirect(['view', 'id' => $model->idtestimoni]);
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'upload' => $upload,
        ]);
    }

    /**
     * Updates an existing Testimoni model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $date = date('YmdHis');
        $model = $this->findModel($id);
        $upload = new UploadImage();

        if ($model->load(Yii::$app->request->post())) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            //check if file not empty
            if(!empty($upload->imageFile) && $upload->imageFile->size !== 0) {
                //rename filename
                $filename = $upload->removeSpecialChar($upload->imageFile->baseName);
                //upload image
                $modelimage = $upload->uploadImg($model->image,$date,$filename,'img/testimoni/');
                if ($modelimage==true) {
                    //save image
                    $model->image = $filename . $date . '.' . $upload->imageFile->extension;
                }
            }else{
                $model->image = $model->image;
            }
            $model->dateupdate = $date;
            $model->save();
            // return $this->redirect(['view', 'id' => $model->idtestimoni]);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'upload' => $upload,
        ]);
    }

    /**
     * Deletes an existing Testimoni model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = Testimoni::findOne($id);
        UploadImage::unLinkImage(Yii::$app->basePath . '/web/img/testimoni/'.$model->image);
        $model->delete();
        //$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Testimoni model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Testimoni the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Testimoni::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
