<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\LoginForm;
use app\models\User;
use app\models\Account;
use app\models\Profile;
use app\modules\admin\models\Language;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
	public function init() {

	    parent::init();
	    // $this->layout = '@app/modules/admin/views/layouts/main';
	    // custom initialization code goes here
	}

    public function actionError()
    {
        return $this->render('error');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        
        if (!$session->has('language')) {
            $session->set('language', 'fr');
        }
        
        $profile = Profile::findOne(['idaccount'=>Yii::$app->user->id]);
        if (!empty($profile)) {
            $session->set('username',$profile->name);
            $name = $profile->name;
        }else{
            $name = '';
        }
        
        return $this->render('index',[
            'lang'=>$session->get('language'),
            'name'=>$name,
        ]);
    }

    public function actionLanguage()
    {
        $language = new Language();
        $session = Yii::$app->session;
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        
        $alert='';
        if ($language->load(Yii::$app->request->post())) {
            $session->set('language', $language->lang);
            $alert = '<div class="alert alert-success alert-dismissible fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Success!</h4>Success to Change Language.</div>';
        }
        if ($language->lang) {
            $session->set('language', $language->lang);
        }
        return $this->render('language',[
            'alert'=>$alert,
            'language'=>$language,]);
    }

    public function actionRegister()
    {
        $this->layout = "@app/modules/admin/views/layouts/main-login";

        $account = new Account();
        $profile = new Profile();
        if ($account->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            $account->role = 2;
            $account->status = 1;
            if($account->save()){
                $update = Account::findOne(['email'=>$account->email]);
                $update->password = md5($account->password);
                $update->passwordrepeat = md5($account->passwordrepeat);
                $update->save();
                $profile->idaccount = $account->idaccount;
                if($profile->save()){
                    return $this->redirect(['login']);
                }
            }
            $account->password='';
            $account->passwordrepeat='';
        }
        return $this->render('register',[
            'account'=>$account,
            'profile'=>$profile,
        ]);
    }

    public function actionLogin()
    {
        $this->layout = "@app/modules/admin/views/layouts/main-login";
        $session = Yii::$app->session;
        $session->open();
        
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['index']);
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $command = Yii::$app->db->createCommand();
            $command->update('account',
                ['lastlogin'=>date('Y-m-d H:i:s')],//update lastlogin
                ['email'=>$model->username])->execute();
            $profile = Profile::findOne(['idaccount'=>Yii::$app->user->id]);
            $session->set('username',$profile->name);
            return $this->redirect(['index']);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        $session = Yii::$app->session;

        // destroys all data registered to a session.
        $session->destroy();
        // close a session
        $session->close();
        return $this->redirect(['login']);
    }
}
