<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Food;
use app\models\Foodcat;
use app\models\Foodtype;
use app\modules\admin\models\UploadImage;
use yii\web\UploadedFile;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

class FoodController extends \yii\web\Controller
{
    private function Session(){
        return Yii::$app->session; // start session
    }
    private function Pagesize(){
        return 10; //page size 10
    }
	public function init() 
	{
	    parent::init();
	}

    public function beforeAction($action)
    {
        // check if not login
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['default/login']);
        }
        return parent::beforeAction($action);
    }

	/* Food */
    public function actionIndex()
    {
        // model food for search filter
        $food = new Food();
        // query for active data reader
    	$query = Food::find()
            ->where(['lang'=>$this->session()->get('language')]);
        // check if get search filter
        if ($food->load(Yii::$app->request->get())) {
            
            if ($food->idfoodtype!='' && $food->idfoodcat!='') {
                $query->andWhere(['idfoodtype'=>$food->idfoodtype,'idfoodcat'=>$food->idfoodcat]);
            }
            if ($food->idfoodtype!='' && $food->idfoodcat=='') {
                $query->andWhere(['idfoodtype'=>$food->idfoodtype]);
            }
            if ($food->idfoodcat!='' && $food->idfoodtype=='') {
                $query->andWhere(['idfoodcat'=>$food->idfoodcat]);
            }
        }
        // model for filter category
    	$cat = Foodcat::find()
            ->where(['lang'=>$this->session()->get('language')])->all();
        // model for filter type
    	$type = Foodtype::find()
            ->where(['lang'=>$this->session()->get('language')])->all();
        // active data provider
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pagesize(),
            ],
        ]);

        return $this->render('index',['food'=>$food,'cat'=>$cat,'type'=>$type,'dataProvider'=>$dataProvider]);
    }
    public function actionFoodAdd()
    {
        // datetime now
    	$date = date('YmdHis');
        // new record model
    	$model = new Food();
        // model upload image
    	$upload = new UploadImage();
        // check if post data
        if ($model->load(Yii::$app->request->post())) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            //check if file not empty
            if(!empty($upload->imageFile) && $upload->imageFile->size !== 0) {
                //rename filename
                $filename = $upload->removeSpecialChar($upload->imageFile->baseName);
                //upload image
                $modelimage = $upload->uploadImg($model->image,$date,$filename,'img/admin/food/');
		        if ($modelimage==true) {
                    //save image
		            $model->image = $filename . $date . '.' . $upload->imageFile->extension;
		        }
            }
            //default language if empty
            if (empty($model->lang)) {
                // value default fr (french)
                $model->lang = 'fr';
                $model->country = 'FR';
            }
            // save model
            $model->save();
            // query food
            $query = Food::find()
                ->where(['lang'=>$this->session()->get('language')]);
            // count query
            $count = $query->count();
            // check modulus 
            if ($count % $pagesize == 0) {
                $max_page = ($count - $count % $pagesize) / $pagesize;
            }elseif($count % $pagesize > 0){
                $max_page = ($count - $count % $pagesize) / $pagesize + 1;
            }else{
                $max_page = ($count - $count % $pagesize) / $pagesize + 1;
            }
            // redirect function index
        	return $this->redirect(['index','page'=>$max_page]);
        }
        // model catergory
    	$cat = Foodcat::find()->all();
        // model type
    	$type = Foodtype::find()->all();
    	return $this->render('_form_food',['model'=>$model,'cat'=>$cat,'type'=>$type,'upload'=>$upload,'title'=>'Add Food']);
    }
    public function actionFoodUpdate($id)
    {
        // check if set id
        if (!isset($id)) {
            return $this->redirect(['index']);
        }
        // datetime now
    	$date = date('YmdHis');
        // find one model by id
    	$model = Food::findOne($id);
        // model upload image
    	$upload = new UploadImage();
        //check if post data
    	if ($model->load(Yii::$app->request->post())) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            //check if file not empty
            if(!empty($upload->imageFile) && $upload->imageFile->size !== 0) {
                //rename filename
                $filename = $upload->removeSpecialChar($upload->imageFile->baseName);
                //upload image
                $modelimage = $upload->uploadImg($model->image,$date,$filename,'img/admin/food/');
		        if ($modelimage==true) {
                    //rename image
		            $model->image = $filename . $date . '.' . $upload->imageFile->extension;
		        }
            }else{
                //no rename image
            	$model->image = $model->image;
            }
            $model->save();
        	return $this->redirect(['index']);
        }
        // model catergory
        $cat = Foodcat::find()->all();
        // model type
        $type = Foodtype::find()->all();
    	return $this->render('_form_food',['model'=>$model,'cat'=>$cat,'type'=>$type,'upload'=>$upload,'title'=>'Update Food']);
    }
    public function actionFoodDelete($id)
    {
        // model find one by id
    	$model = Food::findOne($id);
        // check if has image name
        if(!empty($model->image)){
            // unlink or delete file
            UploadImage::unLinkImage(Yii::$app->basePath . '/web/img/admin/food/' . $model->image);
        }
    	// delete model
        $model->delete();
    	return $this->redirect(['index']);
    }

    /* Food Category*/
    public function actionFoodCat()
    {
        // new model
        $model = new Foodcat();
        // check if post data
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }
        // model for data provider
    	$condition = Foodcat::find()
    		->where(['lang'=>$this->session()->get('language')]);
        // active data provider
        $dataProvider = new ActiveDataProvider([
            'query' => $condition,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    	return $this->render('food-cat',['dataProvider'=>$dataProvider,'model'=>$model]);
    }
    public function actionFoodCatUpdate($id)
    {
    	$model = Foodcat::findOne($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect(['food-cat']);
        }
    	return $this->render('_form_food-cat',['model'=>$model]);
    }
    public function actionFoodCatDelete($id)
    {
        // model find one by id
    	$models = Foodcat::findOne($id);
        // delete model
    	$models->delete();
    	return $this->redirect(['food-cat']);
    }
    /* Food Type*/
    public function actionFoodType()
    {
        // new model
    	$model = new Foodtype();
        // check if post data
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }
        // find model
        $condition = Foodtype::find()
            ->where(['lang'=>$this->session()->get('language')]);
        // active data provider
        $dataProvider = new ActiveDataProvider([
            'query' => $condition,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    	return $this->render('food-type',['model'=>$model,'dataProvider'=>$dataProvider]);
    }
    public function actionFoodTypeUpdate($id)
    {
        // model find one by id
        $model = Foodtype::findOne($id);
        // check if post data
    	if ($model->load(Yii::$app->request->post())) {
	    	$model->save();
    		
    		return $this->redirect(['food-type']);
    	}
    	return $this->render('_form_food-type',['model'=>$model]);
    }
    public function actionFoodTypeDelete($id)
    {
        // model find one by id
    	$models = Foodtype::findOne($id);
    	// delete model
    	$models->delete();
    	return $this->redirect(['food-type']);
    }

}
