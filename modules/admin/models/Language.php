<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\web\UploadedFile;

class Language extends Model
{
    /**
     * @var UploadedFile
     */
    public $lang;

    public function rules()
    {
        return [
            [['lang'], 'string', 'skipOnEmpty' => false],
        ];
    }
}