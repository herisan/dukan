<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadImage extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs('img/admin/food/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }

    public function uploadImg($file,$date,$filename,$folder)
    {
        if ($this->validate()) {
            if(!empty($file))
            {
                UploadImage::unLinkImage(Yii::$app->basePath . '/web/'. $folder . $file);
            }
            //save to folder inside web/
            $this->imageFile->saveAs($folder . $filename . $date . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
        
    }

    public function unLinkImage($url)
    {
        if (file_exists($url)) {
            unlink($url);
        }
    }

    public function removeSpecialChar($filename)
    {
        //replace space with -
        $filename = str_replace(' ', '-', $filename);
        //remove all special characters
        $filename = preg_replace('/[^A-Za-z0-9\-]/', '', $filename);
        return $filename;
    }
}