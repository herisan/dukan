<?php

namespace app\modules\v1\controllers;

use app\components\ApiController;
use app\helpers\DateHelper;
use app\helpers\DBHelper;
use app\helpers\RoleManager;
use app\models\Profile;
use app\models\User;
use Yii;
use yii\web\UnprocessableEntityHttpException;

/**
 * Description of UserController
 *
 * @author Eko Wahyu Wibowo <bowpunya@gmail.com>
 */
class UserController extends ApiController {

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = ['create', 'forgot'];
        return $behaviors;
    }
    
    public function actionCreate() {
        $data = Yii::$app->request->bodyParams;

        $acc = new User();
        $acc->load($data, '');
        $acc->role = RoleManager::ROLE_USER;
        $acc->status = RoleManager::STAT_ACTIVE;
	$acc->lastlogin = DateHelper::now();
        $acc->setPassword($acc->password, true);
        
        if ($acc->save()) {
            $profile = new Profile();
            $profile->load($data, '');
            $profile->idprofile = $acc->idaccount;
            $profile->idaccount = $acc->idaccount;
            $profile->image = "default.jpg";
            if ($profile->save()) {
               // NotifHelper::mailRegister($acc->idaccount);
                return User::findIdentityWihtAccessToken($acc->idaccount);
}
            $acc->delete();
            throw new UnprocessableEntityHttpException(DBHelper::modelErrorsResume($profile));
        }

        throw new UnprocessableEntityHttpException(DBHelper::modelErrorsResume($acc));
    }
//
//    public function actionUpdate() {
//        $data = Yii::$app->request->bodyParams;
//        $acc = User::findOne(RoleManager::identity()->idaccount);
//        $acc->setNewPassword(['newpassword' => isset($data['password']) ? $data['password'] : NULL], true);
//        if ($acc->save()) {
//            $profile = Profile::findOne(['idaccount' => $acc->idaccount]);
//            $profile->load($data['profile'], '');
//            $profile->idaccount = $acc->idaccount;
//            if ($profile->save()) {
//                return User::findIdentityWihtAccessToken($acc->idaccount);
//            }
//            throw new UnprocessableEntityHttpException(Helper::modelErrorsResume($profile));
//        }
//
//        throw new UnprocessableEntityHttpException(Helper::modelErrorsResume($acc));
//    }

    public function actionToken() {
        return ['ok'];
//        $data = Yii::$app->request->bodyParams;
//        $model = new Device;
//        $model->idaccount = RoleManager::identity()->idaccount;
//        $model->token = $data['token'];
//        $model->imei = $data['imei'];
//        return $model->save() ? User::findOne($model->idaccount) : NULL;
    }
//
//    public function actionForgot() {
//        $data = Yii::$app->request->bodyParams;
//        if(!isset($data['username'])){
//            return NULL;
//        }
//        $model = User::findOne(['username' => $data['username']]);
//        
//        if($model && $model->idrole == 20){
//            $token = User::generateToken($model->idaccount, time() + (1000 * 3600 * 24),Yii::$app->params['secretPub']);
//            if(!NotifHelper::mailForgot($model->profiles[0]->email, $token)){
//                throw new \yii\web\ServerErrorHttpException("Sistem tidak dapat mengirim email, pastikan Email yang Anda gunakan mendaftar benar atau laporkan pada Kantor Cabang DLU terdekat.");
//            }
//            return $model;
//        }else{
//            throw new \yii\web\NotFoundHttpException( "Username ". $data['username'] . " tidak ditemukan!");
//        }
//        
//    }
//
//    public function actionMe() {
//        $id = RoleManager::identity()->idaccount;
//        return User::findOne($id);
//    }

}
