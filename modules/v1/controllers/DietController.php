<?php

namespace app\modules\v1\controllers;

use app\components\ApiController;
use app\helpers\DietHelper;
use Yii;

/**
 * Description of UserController
 *
 * @author Eko Wahyu Wibowo <bowpunya@gmail.com>
 */
class DietController extends ApiController {

    public function actionCalc(){
        $data = Yii::$app->request->bodyParams;
        
        return DietHelper::calcJustweight($data);
        
    }

}
