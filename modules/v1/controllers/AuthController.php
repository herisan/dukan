<?php

namespace app\modules\v1\controllers;

use app\helpers\DateHelper;
use app\models\User;
use Yii;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\Controller;
use yii\web\UnauthorizedHttpException;



class AuthController extends Controller
{
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['basicAuth'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => function ($username, $password) {
                $user = User::findByUsername($username);
                if ($user && $user->validatePassword($password)) {
                    return $user;
                }
                return null;
            },
        ];
        return $behaviors;
    }

    public function actionIndex() {
        $id = Yii::$app->user->identity->idaccount;
        $user = User::findIdentityWihtAccessToken($id);
        if ($user) {
		$user->lastlogin = DateHelper::now();
       		$user->passwordrepeat = $user->password;
		$user->save();
            	return $user;
        }
        throw new UnauthorizedHttpException("User not allowed.");
    }
    
}
