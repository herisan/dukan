<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property int $idprofile
 * @property string $name
 * @property string $fullname
 * @property string $phone
 * @property string $postalcode
 * @property string $address
 * @property string $city
 * @property string $country
 * @property string $gender
 * @property string $dob
 * @property string $motivation
 * @property string $relationship
 * @property int $children
 * @property string $registerdate
 * @property string $image
 * @property int $idaccount
 *
 * @property Account $account
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'gender', 'idaccount'], 'required'],
            [['dob', 'registerdate'], 'safe'],
            [['children', 'idaccount'], 'integer'],
            [['name', 'fullname', 'city', 'country'], 'string', 'max' => 45],
            [['phone'], 'string', 'max' => 16],
            [['postalcode'], 'string', 'max' => 8],
            [['address'], 'string', 'max' => 250],
            [['gender'], 'string', 'max' => 1],
            [['motivation'], 'string', 'max' => 500],
            [['relationship'], 'string', 'max' => 25],
            [['image'], 'string', 'max' => 150],
            [['idaccount'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['idaccount' => 'idaccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idprofile' => Yii::t('app', 'Idprofile'),
            'name' => Yii::t('app', 'Name'),
            'fullname' => Yii::t('app', 'Fullname'),
            'phone' => Yii::t('app', 'Phone'),
            'postalcode' => Yii::t('app', 'Postalcode'),
            'address' => Yii::t('app', 'Address'),
            'city' => Yii::t('app', 'City'),
            'country' => Yii::t('app', 'Country'),
            'gender' => Yii::t('app', 'Gender'),
            'dob' => Yii::t('app', 'Dob'),
            'motivation' => Yii::t('app', 'Motivation'),
            'relationship' => Yii::t('app', 'Relationship'),
            'children' => Yii::t('app', 'Children'),
            'registerdate' => Yii::t('app', 'Registerdate'),
            'image' => Yii::t('app', 'Image'),
            'idaccount' => Yii::t('app', 'Idaccount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['idaccount' => 'idaccount']);
    }
}
