<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscription".
 *
 * @property int $idsubscription
 * @property int $subscriptiontype
 * @property string $createdate
 * @property string $duedate
 * @property int $status
 * @property int $idaccount
 * @property int $idpayment
 *
 * @property Diet[] $diets
 * @property Account $account
 * @property Payment $payment
 */
class Subscription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subscriptiontype', 'status', 'idaccount', 'idpayment'], 'integer'],
            [['createdate', 'duedate'], 'safe'],
            [['idaccount', 'idpayment'], 'required'],
            [['idaccount'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['idaccount' => 'idaccount']],
            [['idpayment'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::className(), 'targetAttribute' => ['idpayment' => 'idpayment']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idsubscription' => Yii::t('app', 'Idsubscription'),
            'subscriptiontype' => Yii::t('app', 'Subscriptiontype'),
            'createdate' => Yii::t('app', 'Createdate'),
            'duedate' => Yii::t('app', 'Duedate'),
            'status' => Yii::t('app', 'Status'),
            'idaccount' => Yii::t('app', 'Idaccount'),
            'idpayment' => Yii::t('app', 'Idpayment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiets()
    {
        return $this->hasMany(Diet::className(), ['idsubscription' => 'idsubscription']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['idaccount' => 'idaccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['idpayment' => 'idpayment']);
    }
}
