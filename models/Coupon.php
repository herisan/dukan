<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coupon".
 *
 * @property int $idcoupon
 * @property int $idrefer
 * @property int $coupontype
 * @property string $createdate
 * @property string $duedate
 * @property string $code
 * @property string $label
 * @property int $day
 * @property double $amount
 * @property int $useby
 * @property string $usedate
 * @property int $status
 */
class Coupon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coupon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idrefer', 'coupontype', 'day', 'useby', 'status'], 'integer'],
            [['createdate', 'duedate', 'usedate'], 'safe'],
            [['amount'], 'number'],
            [['code'], 'string', 'max' => 16],
            [['label'], 'string', 'max' => 45],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcoupon' => Yii::t('app', 'Idcoupon'),
            'idrefer' => Yii::t('app', 'Idrefer'),
            'coupontype' => Yii::t('app', 'Coupontype'),
            'createdate' => Yii::t('app', 'Createdate'),
            'duedate' => Yii::t('app', 'Duedate'),
            'code' => Yii::t('app', 'Code'),
            'label' => Yii::t('app', 'Label'),
            'day' => Yii::t('app', 'Day'),
            'amount' => Yii::t('app', 'Amount'),
            'useby' => Yii::t('app', 'Useby'),
            'usedate' => Yii::t('app', 'Usedate'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
