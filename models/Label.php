<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "label".
 *
 * @property int $idlabel
 * @property string $key
 * @property string $val
 * @property string $lang
 */
class Label extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'label';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idlabel'], 'required'],
            [['idlabel'], 'integer'],
            [['val'], 'string'],
            [['key'], 'string', 'max' => 45],
            [['lang'], 'string', 'max' => 4],
            [['idlabel'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idlabel' => Yii::t('app', 'Idlabel'),
            'key' => Yii::t('app', 'Key'),
            'val' => Yii::t('app', 'Val'),
            'lang' => Yii::t('app', 'Lang'),
        ];
    }
}
