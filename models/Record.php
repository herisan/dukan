<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "record".
 *
 * @property int $idrecord
 * @property int $type
 * @property int $day
 * @property string $dateinput
 * @property string $date
 * @property int $method
 * @property int $phase
 * @property int $format
 * @property string $color
 * @property double $wtarget
 * @property double $wreal
 * @property int $starget
 * @property int $sreal
 * @property int $pabdo
 * @property int $pback
 * @property int $pchest
 * @property int $dtarget
 * @property int $dreal
 * @property double $sontarget
 * @property double $sonreal
 * @property int $motivation
 * @property int $emotion
 * @property string $fault
 * @property int $idaccount
 * @property int $idcalc
 * @property int $iddiet
 *
 * @property Account $account
 * @property Calc $calc
 * @property Diet $diet
 */
class Record extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'record';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idrecord', 'type', 'day', 'method', 'phase', 'format', 'wtarget', 'starget', 'dtarget', 'sontarget', 'idaccount', 'idcalc', 'iddiet'], 'required'],
            [['idrecord', 'type', 'day', 'method', 'phase', 'format', 'starget', 'sreal', 'pabdo', 'pback', 'pchest', 'dtarget', 'dreal', 'motivation', 'emotion', 'idaccount', 'idcalc', 'iddiet'], 'integer'],
            [['dateinput', 'date'], 'safe'],
            [['wtarget', 'wreal', 'sontarget', 'sonreal'], 'number'],
            [['color'], 'string', 'max' => 12],
            [['fault'], 'string', 'max' => 45],
            [['idrecord'], 'unique'],
            [['idaccount'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['idaccount' => 'idaccount']],
            [['idcalc'], 'exist', 'skipOnError' => true, 'targetClass' => Calc::className(), 'targetAttribute' => ['idcalc' => 'idcalc']],
            [['iddiet'], 'exist', 'skipOnError' => true, 'targetClass' => Diet::className(), 'targetAttribute' => ['iddiet' => 'iddiet']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idrecord' => Yii::t('app', 'Idrecord'),
            'type' => Yii::t('app', 'Type'),
            'day' => Yii::t('app', 'Day'),
            'dateinput' => Yii::t('app', 'Dateinput'),
            'date' => Yii::t('app', 'Date'),
            'method' => Yii::t('app', 'Method'),
            'phase' => Yii::t('app', 'Phase'),
            'format' => Yii::t('app', 'Format'),
            'color' => Yii::t('app', 'Color'),
            'wtarget' => Yii::t('app', 'Wtarget'),
            'wreal' => Yii::t('app', 'Wreal'),
            'starget' => Yii::t('app', 'Starget'),
            'sreal' => Yii::t('app', 'Sreal'),
            'pabdo' => Yii::t('app', 'Pabdo'),
            'pback' => Yii::t('app', 'Pback'),
            'pchest' => Yii::t('app', 'Pchest'),
            'dtarget' => Yii::t('app', 'Dtarget'),
            'dreal' => Yii::t('app', 'Dreal'),
            'sontarget' => Yii::t('app', 'Sontarget'),
            'sonreal' => Yii::t('app', 'Sonreal'),
            'motivation' => Yii::t('app', 'Motivation'),
            'emotion' => Yii::t('app', 'Emotion'),
            'fault' => Yii::t('app', 'Fault'),
            'idaccount' => Yii::t('app', 'Idaccount'),
            'idcalc' => Yii::t('app', 'Idcalc'),
            'iddiet' => Yii::t('app', 'Iddiet'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['idaccount' => 'idaccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalc()
    {
        return $this->hasOne(Calc::className(), ['idcalc' => 'idcalc']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiet()
    {
        return $this->hasOne(Diet::className(), ['iddiet' => 'iddiet']);
    }
}
