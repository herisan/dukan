<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "food".
 *
 * @property int $idfood
 * @property string $name
 * @property int $method
 * @property int $phase
 * @property string $image
 * @property string $desc
 * @property string $lang
 * @property string $country
 * @property int $idfoodtype
 * @property int $idfoodcat
 *
 * @property Foodcat $foodcat
 * @property Foodtype $foodtype
 */
class Food extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'food';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idfood', 'name', 'idfoodtype', 'idfoodcat'], 'required'],
            [['idfood','method', 'phase', 'idfoodtype', 'idfoodcat'], 'integer'],
            [['desc'], 'string'],
            [['name'], 'string', 'max' => 45],
            [['image'], 'string', 'max' => 1024],
            [['lang', 'country'], 'string', 'max' => 4],
            [['idfood'], 'unique'],
            [['idfoodcat'], 'exist', 'skipOnError' => true, 'targetClass' => Foodcat::className(), 'targetAttribute' => ['idfoodcat' => 'idfoodcat']],
            [['idfoodtype'], 'exist', 'skipOnError' => true, 'targetClass' => Foodtype::className(), 'targetAttribute' => ['idfoodtype' => 'idfoodtype']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idfood' => Yii::t('app', 'Idfood'),
            'name' => Yii::t('app', 'Name'),
            'method' => Yii::t('app', 'Method'),
            'phase' => Yii::t('app', 'Phase'),
            'image' => Yii::t('app', 'Image'),
            'desc' => Yii::t('app', 'Desc'),
            'lang' => Yii::t('app', 'Lang'),
            'country' => Yii::t('app', 'Country'),
            'idfoodtype' => Yii::t('app', 'Idfoodtype'),
            'idfoodcat' => Yii::t('app', 'Idfoodcat'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodcat()
    {
        return $this->hasOne(Foodcat::className(), ['idfoodcat' => 'idfoodcat']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodtype()
    {
        return $this->hasOne(Foodtype::className(), ['idfoodtype' => 'idfoodtype']);
    }

    public function getMethod($id)
    {
        switch ($id) {
            case '1':
                $result = "PRÉSENTATION DE LA MÉTHODE CLASSIQUE";
                break;
            
            default:
                $result = "PRÉSENTATION DE L’ESCALIER NUTRITIONNEL";
                break;
        }
        return $result;
    }
    public function getMethodShort($id)
    {
        switch ($id) {
            case '1':
                $result = "Méthode Classique";
                break;
            
            default:
                $result = "Escalier Nutritionnel";
                break;
        }
        return $result;
    }
    public function getPhase($id)
    {
        switch ($id) {
            case '1':
                $result = "ATTAQUE";
                break;
            
            case '2':
                $result = "CROISIÈRE";
                break;
            
            case '3':
                $result = "CONSOLIDATION";
                break;
            
            default:
                $result = "STABILISATION";
                break;
        }
        return $result;
    }
}
