<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property int $idlog
 * @property string $date
 * @property string $content
 * @property int $idaccount
 *
 * @property Account $account
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['content'], 'string'],
            [['idaccount'], 'required'],
            [['idaccount'], 'integer'],
            [['idaccount'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['idaccount' => 'idaccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idlog' => Yii::t('app', 'Idlog'),
            'date' => Yii::t('app', 'Date'),
            'content' => Yii::t('app', 'Content'),
            'idaccount' => Yii::t('app', 'Idaccount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['idaccount' => 'idaccount']);
    }
}
