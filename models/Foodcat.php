<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "foodcat".
 *
 * @property int $idfoodcat
 * @property string $name
 *
 * @property Food[] $foods
 */
class Foodcat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'foodcat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idfoodcat', 'name'], 'required'],
            [['idfoodcat'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['lang'], 'string', 'max' => 50],
            [['idfoodcat'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idfoodcat' => Yii::t('app', 'Idfoodcat'),
            'name' => Yii::t('app', 'Name'),
            'lang' => Yii::t('app', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoods()
    {
        return $this->hasMany(Food::className(), ['idfoodcat' => 'idfoodcat']);
    }
}
