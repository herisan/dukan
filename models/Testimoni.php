<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "testimoni".
 *
 * @property int $idtestimoni
 * @property string $method
 * @property string $phase
 * @property string $image
 * @property string $desc
 * @property string $lang
 * @property string $age
 * @property int $wstart
 * @property int $wlast
 * @property string $dateinput
 * @property string $dateupdate
 */
class Testimoni extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'testimoni';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['method', 'phase', 'image', 'desc', 'lang', 'age', 'dateinput'], 'required'],
            [['desc'], 'string'],
            [['wstart', 'wlast'], 'integer'],
            [['dateinput', 'dateupdate'], 'safe'],
            [['method', 'phase', 'lang', 'age'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtestimoni' => 'Idtestimoni',
            'method' => 'Method',
            'phase' => 'Phase',
            'image' => 'Image',
            'desc' => 'Desc',
            'lang' => 'Lang',
            'age' => 'Age',
            'wstart' => 'Wstart',
            'wlast' => 'Wlast',
            'dateinput' => 'Dateinput',
            'dateupdate' => 'Dateupdate',
        ];
    }
}
