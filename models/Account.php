<?php

namespace app\models;

use app\helpers\RoleManager;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "account".
 *
 * @property int $idaccount
 * @property string $email
 * @property string $password
 * @property int $role
 * @property string $lastlogin
 * @property int $status
 *
 * @property Calc[] $calcs
 * @property Config[] $configs
 * @property Payment[] $payments
 * @property Profile[] $profiles
 * @property Record[] $records
 * @property Subscription[] $subscriptions
 */
class Account extends ActiveRecord
{
    
    
    public $passwordrepeat;
    public $newpassword;
    public $newpasswordrepeat;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'account';
    }

    
    public function rules() {
        return [
            [['email', 'password', 'passwordrepeat', 'role', 'status'], 'required'],
            [['role', 'status'], 'integer'],
            [['email'], 'email'],
            [['lastlogin'], 'safe'],
            [['email'], 'string', 'min' => 8,'max' => 250],
           // [['username'], 'match', 'not' => true, 'pattern' => '/[^@a-z0-9_.]/', 'message' => 'Username hanya boleh mengandung huruf kecil, angka dan titik (.).'],
            [['password', 'passwordrepeat', 'newpassword', 'newpasswordrepeat'], 'string', 'min' => 6, 'max' => 100],
            [['passwordrepeat'], 'compare', 'compareAttribute' => 'password', 'message' => 'Konfirmasi password tidak cocok!'],
            [['newpasswordrepeat'], 'compare', 'compareAttribute' => 'newpassword', 'message' => 'Konfirmasi password baru tidak cocok!'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idaccount' => Yii::t('app', 'Idaccount'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'role' => Yii::t('app', 'Role'),
            'lastlogin' => Yii::t('app', 'Lastlogin'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
    
    public function getSubscription(){
        $sub = Subscription::findOne(['idaccount' => $this->idaccount, 'status' => RoleManager::STAT_ACTIVE]);
        return $sub;
    }
    
    /**
     * @return ActiveQuery
     */
    public function getCalcs()
    {
        return $this->hasMany(Calc::className(), ['idaccount' => 'idaccount']);
    }

    /**
     * @return ActiveQuery
     */
    public function getConfigs()
    {
        return $this->hasMany(Config::className(), ['idaccount' => 'idaccount']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['idaccount' => 'idaccount']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['idaccount' => 'idaccount']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRecords()
    {
        return $this->hasMany(Record::className(), ['idaccount' => 'idaccount']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['idaccount' => 'idaccount']);
    }
}
