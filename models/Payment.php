<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property int $idpayment
 * @property string $idrefer
 * @property string $code
 * @property string $date
 * @property int $day
 * @property double $amount
 * @property int $status
 * @property string $validby
 * @property string $validdate
 * @property string $log
 * @property int $idaccount
 *
 * @property Account $account
 * @property Subscription[] $subscriptions
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'validdate'], 'safe'],
            [['day', 'status', 'idaccount'], 'integer'],
            [['amount'], 'number'],
            [['log'], 'string'],
            [['idaccount'], 'required'],
            [['idrefer', 'validby'], 'string', 'max' => 45],
            [['code'], 'string', 'max' => 8],
            [['idaccount'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['idaccount' => 'idaccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idpayment' => Yii::t('app', 'Idpayment'),
            'idrefer' => Yii::t('app', 'Idrefer'),
            'code' => Yii::t('app', 'Code'),
            'date' => Yii::t('app', 'Date'),
            'day' => Yii::t('app', 'Day'),
            'amount' => Yii::t('app', 'Amount'),
            'status' => Yii::t('app', 'Status'),
            'validby' => Yii::t('app', 'Validby'),
            'validdate' => Yii::t('app', 'Validdate'),
            'log' => Yii::t('app', 'Log'),
            'idaccount' => Yii::t('app', 'Idaccount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['idaccount' => 'idaccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['idpayment' => 'idpayment']);
    }
}
