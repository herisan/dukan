<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "device".
 *
 * @property int $iddevice
 * @property string $imei
 * @property string $token
 * @property string $name
 * @property string $date
 * @property string $expire
 * @property int $idaccount
 *
 * @property Account $account
 */
class Device extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iddevice', 'imei', 'token', 'idaccount'], 'required'],
            [['iddevice', 'idaccount'], 'integer'],
            [['date', 'expire'], 'safe'],
            [['imei', 'name'], 'string', 'max' => 255],
            [['token'], 'string', 'max' => 1024],
            [['token'], 'unique'],
            [['imei'], 'unique'],
            [['iddevice'], 'unique'],
            [['idaccount'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['idaccount' => 'idaccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddevice' => Yii::t('app', 'Iddevice'),
            'imei' => Yii::t('app', 'Imei'),
            'token' => Yii::t('app', 'Token'),
            'name' => Yii::t('app', 'Name'),
            'date' => Yii::t('app', 'Date'),
            'expire' => Yii::t('app', 'Expire'),
            'idaccount' => Yii::t('app', 'Idaccount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['idaccount' => 'idaccount']);
    }
}
