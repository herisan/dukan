<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calc".
 *
 * @property int $idcalc
 * @property int $system
 * @property double $wactual
 * @property double $wdesire
 * @property double $wavg
 * @property double $wmax
 * @property double $wmin
 * @property double $height
 * @property string $dob
 * @property int $gene
 * @property int $diet
 * @property int $bone
 * @property string $gender
 * @property int $pregnancy
 * @property double $result
 * @property int $idaccount
 * @property int $score
 * @property int $act
 * @property string $message
 *
 * @property Account $account
 * @property Record[] $records
 */
class Calc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'idaccount'], 'required'],
            [['idcalc', 'system', 'gene', 'diet','bone', 'pregnancy', 'idaccount', 'score', 'act'], 'integer'],
            [['wactual', 'wdesire', 'wmax', 'wmin', 'height', 'result'], 'number'],
            [['dob', 'message'], 'safe'],
            [['gender'], 'string', 'max' => 1],
            [['idcalc'], 'unique'],
            [['idaccount'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['idaccount' => 'idaccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcalc' => Yii::t('app', 'Idcalc'),
            'system' => Yii::t('app', 'System'),
            'wactual' => Yii::t('app', 'Wactual'),
            'wdesire' => Yii::t('app', 'Wdesire'),
            'wmax' => Yii::t('app', 'Wmax'),
            'wmin' => Yii::t('app', 'Wmin'),
            'height' => Yii::t('app', 'Height'),
            'dob' => Yii::t('app', 'Dob'),
            'gene' => Yii::t('app', 'Gene'),
            'hand' => Yii::t('app', 'Hand'),
            'gender' => Yii::t('app', 'Gender'),
            'pregnancy' => Yii::t('app', 'Pregnancy'),
            'result' => Yii::t('app', 'Result'),
            'idaccount' => Yii::t('app', 'Idaccount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['idaccount' => 'idaccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecords()
    {
        return $this->hasMany(Record::className(), ['idcalc' => 'idcalc']);
    }
}
