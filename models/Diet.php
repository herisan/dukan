<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "diet".
 *
 * @property int $iddiet
 * @property string $date
 * @property int $method
 * @property int $phase
 * @property int $day
 * @property int $warn
 * @property string $startdate
 * @property string $enddate
 * @property int $idaccount
 * @property int $idsubscription
 *
 * @property Account $account
 * @property Subscription $subscription
 * @property Record[] $records
 */
class Diet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'diet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iddiet', 'date', 'method', 'phase', 'day', 'idaccount', 'idsubscription'], 'required'],
            [['iddiet', 'method', 'phase', 'day', 'warn', 'idaccount', 'idsubscription'], 'integer'],
            [['date', 'startdate', 'enddate'], 'safe'],
            [['iddiet'], 'unique'],
            [['idaccount'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['idaccount' => 'idaccount']],
            [['idsubscription'], 'exist', 'skipOnError' => true, 'targetClass' => Subscription::className(), 'targetAttribute' => ['idsubscription' => 'idsubscription']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddiet' => Yii::t('app', 'Iddiet'),
            'date' => Yii::t('app', 'Date'),
            'method' => Yii::t('app', 'Method'),
            'phase' => Yii::t('app', 'Phase'),
            'day' => Yii::t('app', 'Day'),
            'warn' => Yii::t('app', 'Warn'),
            'startdate' => Yii::t('app', 'Startdate'),
            'enddate' => Yii::t('app', 'Enddate'),
            'idaccount' => Yii::t('app', 'Idaccount'),
            'idsubscription' => Yii::t('app', 'Idsubscription'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['idaccount' => 'idaccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['idsubscription' => 'idsubscription']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecords()
    {
        return $this->hasMany(Record::className(), ['iddiet' => 'iddiet']);
    }
}
