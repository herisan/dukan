<?php

namespace app\models;

use Exception;
use Firebase\JWT\JWT;
use Yii;
use yii\web\IdentityInterface;

class User extends Account implements IdentityInterface {

    public $authKey;
    public $accessToken;

    public function fields() {
        return array_merge(parent::fields(), ['accessToken', 'authKey', 'profile', 'subscription']);
    }

    public static function generateToken($id, $exp = null, $secret = null) {
        $token = [
            "iss" => "http://lumut.id",
            "aud" => "id.lumut.dukan",
            "uid" => $id,
            "iat" => time(),
            "nbf" => time(),
            "exp" => $exp ? $exp : time() + (1000 * 3600 * 24 * 7)
        ];

        return JWT::encode($token, $secret ? $secret : Yii::$app->params['secret']);
    }
    
    public static function generateStaticToken() {
        $token = [
            "iss" => "http://lumut.id",
            "aud" => "id.lumut.dukan",
            "uid" => 'crm',
            "iat" => time(),
            "nbf" => time()
        ];

        return JWT::encode($token, Yii::$app->params['secret']);
    }
    
    public static function validateToken($token, $secret = null) {
        try {
            $payload = JWT::decode($token, $secret ? $secret : Yii::$app->params['secret'], ['HS256']);
            if($payload->uid === 0){
                $statictoken = new User();
                $statictoken->idaccount = 0;
                return $statictoken;
            }
            $user = User::findIdentity($payload->uid);
            return $user;//$user->idrole == \app\components\Helper::ROLE_UMUM ? $user : NULL;
        } catch (Exception $ex) {
            return null;
        }
    }

    public static function findIdentity($id) {
        return User::findOne(['idaccount' => $id, 'status' => 1]);
    }

    public static function findIdentityWihtAccessToken($id) {
        $user = User::findOne(['idaccount' => $id, 'status' => 1]);
        if ($user) {
            $user->accessToken = self::generateToken($id);
        }
        return $user;
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        if ($type == 'yii\filters\auth\HttpBearerAuth') {
            return User::validateToken($token);
        }

        return null;
    }

    public static function findByUsername($username) {
        return User::findOne(['email' => $username, 'status' => 1]);
    }

    public function getId() {
        return $this->idaccount;
    }

    public function getAuthKey() {
        return $this->authKey;
    }

    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    public function validatePassword($password) {
        $pass = md5($password);

        return $this->password === $pass && $this->status == 1;
    }

    public function setPassword($password, $bypass = false) {
        $pass = md5($password);
        $this->passwordrepeat = ($this->password == $this->passwordrepeat || $bypass) ? $pass : $this->passwordrepeat;
        $this->password = $pass;
    }

    public function setNewPassword($data, $bypass = false) {
        $this->newpassword = isset($data['newpassword']) ? $data['newpassword'] : NULL;
        $this->newpasswordrepeat = isset($data['newpasswordrepeat']) ? $data['newpasswordrepeat'] : NULL;
        $pass = md5($this->newpassword);
        if (!empty($this->newpassword) && ($this->newpassword == $this->newpasswordrepeat || $bypass)) {
            $this->password = $pass;
            $this->passwordrepeat = $pass;
        } else if(!empty($this->newpassword)) {
            $this->password = $pass;
            $this->passwordrepeat = $pass . 'random';
        }else{
            $this->passwordrepeat = $this->password;
        }
    }
    
    public function clearPassword(){
        $this->password = NULL;
        $this->passwordrepeat = NULL;
        $this->newpassword = NULL;
        $this->newpasswordrepeat = NULL;
    }

    public function resetPassword(){
       
    }
    // add checkpass 4-10-2019
    public function checkPass($password)
    {
        $pass = md5($password);
        return User::findOne(['password' => $pass, 'status' => 1]);
    }
    
}
