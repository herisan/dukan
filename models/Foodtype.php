<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "foodtype".
 *
 * @property int $idfoodtype
 * @property string $name
 *
 * @property Food[] $foods
 */
class Foodtype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'foodtype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idfoodtype', 'name'], 'required'],
            [['idfoodtype'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['lang'], 'string', 'max' => 50],
            [['idfoodtype'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idfoodtype' => Yii::t('app', 'Idfoodtype'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoods()
    {
        return $this->hasMany(Food::className(), ['idfoodtype' => 'idfoodtype']);
    }
}
