<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class LoginDukan extends Model
{
    public $username;
    public $password;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are required
            [['username', 'password'], 'required'],
            [['username'], 'string', 'min' => 8,'max' => 250],
            [['password'], 'string', 'min' => 6, 'max' => 100],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        // return [
        //     'verifyCode' => 'Verification Code',
        // ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
}
