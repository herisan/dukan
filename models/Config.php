<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "config".
 *
 * @property int $idconfig
 * @property string $key
 * @property string $val
 * @property int $idaccount
 *
 * @property Account $account
 */
class Config extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idconfig', 'idaccount'], 'required'],
            [['idconfig', 'idaccount'], 'integer'],
            [['val'], 'string'],
            [['key'], 'string', 'max' => 45],
            [['idconfig'], 'unique'],
            [['idaccount'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['idaccount' => 'idaccount']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idconfig' => Yii::t('app', 'Idconfig'),
            'key' => Yii::t('app', 'Key'),
            'val' => Yii::t('app', 'Val'),
            'idaccount' => Yii::t('app', 'Idaccount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['idaccount' => 'idaccount']);
    }
}
