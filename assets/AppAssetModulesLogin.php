<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetModulesLogin extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'vendor/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css',//Bootstrap 3.3.7
        'vendor/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css',//Font Awesome
        'vendor/AdminLTE/bower_components/Ionicons/css/ionicons.min.css',//Ionicons
        'vendor/AdminLTE/dist/css/AdminLTE.min.css',//Theme style
        'vendor/AdminLTE/plugins/iCheck/square/blue.css',//iCheck
    ];
    public $js = [
        'vendor/AdminLTE/bower_components/jquery/dist/jquery.min.js',//jQuery 3
        'vendor/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js',//Bootstrap 3.3.7
        'vendor/AdminLTE/plugins/iCheck/icheck.min.js',//iCheck
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
