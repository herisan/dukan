<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetModules extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'vendor/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css',//Bootstrap 3.3.7
        'vendor/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css',//Font Awesome
        'vendor/AdminLTE/bower_components/Ionicons/css/ionicons.min.css',//Ionicons
        'vendor/AdminLTE/dist/css/AdminLTE.min.css',//Theme style
        'vendor/AdminLTE/dist/css/skins/_all-skins.min.css',//AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load.
        'vendor/AdminLTE/bower_components/morris.js/morris.css',//Morris chart
        'vendor/AdminLTE/bower_components/jvectormap/jquery-jvectormap.css',//jvectormap
        'vendor/AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',//Date Picker
        'vendor/AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.css',//Daterange picker
        'vendor/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',//bootstrap wysihtml5 - text editor
        'vendor/AdminLTE/bootstrap-toggle-master/css/bootstrap-toggle.min.css',
    ];
    public $js = [
        'vendor/AdminLTE/bower_components/jquery/dist/jquery.min.js',//jQuery 3
        'vendor/AdminLTE/bower_components/jquery-ui/jquery-ui.min.js',//jQuery UI 1.11.4
        'vendor/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js',//Bootstrap 3.3.7
        'vendor/AdminLTE/bower_components/raphael/raphael.min.js',//Morris.js charts
        'vendor/AdminLTE/bower_components/morris.js/morris.min.js',//Morris.js charts
        'vendor/AdminLTE/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js',//Sparkline
        'vendor/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',//jvectormap
        'vendor/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',//jvectormap
        'vendor/AdminLTE/bower_components/jquery-knob/dist/jquery.knob.min.js',//jQuery Knob Chart
        'vendor/AdminLTE/bower_components/moment/min/moment.min.js',//daterangepicker
        'vendor/AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.js',//daterangepicker
        'vendor/AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',//datepicker
        'vendor/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',//Bootstrap WYSIHTML5
        'vendor/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',//Slimscroll
        'vendor/AdminLTE/bower_components/fastclick/lib/fastclick.js',//FastClick
        'vendor/AdminLTE/dist/js/adminlte.min.js',//AdminLTE App
        'vendor/AdminLTE/dist/js/pages/dashboard.js',//AdminLTE dashboard demo (This is only for demo purposes)
        'vendor/AdminLTE/dist/js/demo.js',//AdminLTE for demo purposes
        'vendor/AdminLTE/bootstrap-toggle-master/js/bootstrap-toggle.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
