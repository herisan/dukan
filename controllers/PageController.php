<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use app\models\ContactPage;
use app\models\Testimoni;
use app\models\LoginDukan;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

class PageController extends \yii\web\Controller
{
    private function Session()
    {
        return Yii::$app->session;
    }
    public function beforeAction($action)
    {
        if (!$this->session()->has('lang_dukan')) {
            $this->session()->set('lang_dukan','fr');
            $this->session()->set('span_language','CHOISISSEZ VOTRE LANGUE');
        }
        return true;
    }
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionReadmoreTextIntro()
    {
        return $this->render('text-intro');
    }
    public function actionTextesBas()
    {
        return $this->render('textes-bas');
    }

    public function actionTestimoni()
    {
        $pagesize=5;
        $query = Testimoni::find()
            ->where(['lang'=>$this->session()->get('lang_dukan')]);
        // get the total number of articles (but do not fetch the article data yet)
        $count = $query->count();
        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count,'defaultPageSize'=>$pagesize]);

        // $model = $query->all();
        // limit the query using the pagination and retrieve the articles
        $model = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('testimoni',['model'=>$model,'pagination'=>$pagination]);
    }
    public function actionTestimoniReadmore($id)
    {
        $model = Testimoni::findOne($id);
        return $this->render('read-testimoni',['model'=>$model]);
    }

    public function actionContact()
    {
        $contact = new ContactPage();
        if ($contact->load(Yii::$app->request->post())) {
            # code...
        }
        return $this->render('contact',['contact'=>$contact]);
    }

    public function actionConnection()
    {
        return $this->render('navbar/connection');
    }
    public function actionConnectionEmail()
    {
        $model = new LoginDukan();
        if ($model->load(Yii::$app->request->post())) {
            # code...
        }

        return $this->render('navbar/connection-email-phone',['model'=>$model]);
    }

    public function actionFindUsApp()
    {
        return $this->render('navbar/find-us-app');
    }

    public function actionSetSessionLang($lang)
    {
    	$this->session()->set('lang_dukan',$lang);
        $lang = $this->session()->get('lang_dukan');
        switch ($lang) {
            case 'en':
                $this->session()->set('span_language','CHOOSE YOUR LANGUAGE');
                break;
            case 'pr':
                $this->session()->set('span_language','ESCOLHA SUA LÍNGUA');
                break;
            case 'es':
                $this->session()->set('span_language','ELIGE TU IDIOMA');
                break;
            case 'it':
                $this->session()->set('span_language','SCEGLI LA TUA LINGUA');
                break;
            case 'ru':
                $this->session()->set('span_language','ВЫБЕРИТЕ СВОЙ ЯЗЫК');
                break;
            case 'tr':
                $this->session()->set('span_language','DİLİNİZİ SEÇİN');
                break;
            case 'de':
                $this->session()->set('span_language','WÄHLEN SIE IHRE SPRACHE');
                break;
            case 'cn':
                $this->session()->set('span_language','选择你的语言');
                break;
            
            default:
                $this->session()->set('span_language','CHOISISSEZ VOTRE LANGUE');
                break;
        }
    	return $this->redirect(Yii::$app->request->referrer);
    }

    /*public function actionReadJson()
    {
            // \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON; 
        // $json  = file_get_contents(Url::base(true).'/json/lang_dukan.json');
        // $pars = json_encode($json);
        // echo $pars['json'];
    }*/

}
