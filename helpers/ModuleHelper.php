<?php

namespace app\helpers;
use Yii;

class ModuleHelper{
    
    public static function getMethodList() {
        return [
		  '1' => 'PRÉSENTATION DE LA MÉTHODE CLASSIQUE', 
		  '2' => 'PRESENTATION DE L’ESCALIER NUTRITIONNEL',
		];
    }
    public static function getPhaseList(){
    	return [
		  '1' => 'ATTAQUE', 
		  '2' => 'CROISIÈRE',
		  '3' => 'CONSOLIDATION',
		  '4' => 'STABILISATION',
		];
    }
    public static function getLang($lang){
    	$session = Yii::$app->session;
    	switch ($session->get('lang_dukan')) {
    		case 'en':
    			switch ($lang) {
		            case 'en': return 'English'; break;
		            case 'pr': return 'Portuguese'; break;
		            case 'es': return 'Spanish'; break;
		            case 'it': return 'Italian'; break;
		            case 'ru': return 'Russian'; break;
		            case 'tr': return 'Turkish'; break;
		            case 'de': return 'Germany'; break;
		            case 'cn': return 'Chinese'; break;
		            default: return 'French'; break;
		        }
    			break;
    		
    		default:
    			switch ($lang) {
		            case 'en': return 'Anglais'; break;
		            case 'pr': return 'Portugais'; break;
		            case 'es': return 'Espagnol'; break;
		            case 'it': return 'Italien'; break;
		            case 'ru': return 'Russe'; break;
		            case 'tr': return 'Turc'; break;
		            case 'de': return 'Allemand'; break;
		            case 'cn': return 'Chinois'; break;
		            default: return 'Français'; break;
		        }
    			break;
    	}
    	
    }
}
