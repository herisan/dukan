<?php

namespace app\helpers;

class DateHelper{
    
    public static function today() {
        return date('Y-m-d');
    }

    public static function now() {
        return date('Y-m-d H:i:s');
    }
    
    public static function age($dob){
        $age = date_diff(date_create_from_format('Y-m-d',$dob), date_create('now'))->y;
        return floor($age);
    }
}
