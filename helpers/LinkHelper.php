<?php

namespace app\helpers;
use Yii;

class LinkHelper{
    
    public static function nav_active($url) {
        return (Yii::$app->request->url==$url) ? true : false;
    }
    public static function left_bar_active($url){
    	return (Yii::$app->request->url==$url) ? 'class="active"' : '';
    }
}
