<?php

namespace app\helpers;

use app\models\Calc;

class DietHelper {

    const GENDER_MALE = "M";
    const GENDER_FEMALE = "F";
    const GENDER_LADY = "f";

    public static function calcJustweight($data) {

        $model = new Calc();
        $data['dob'] = date_create_from_format("d/m/Y", $data['dob'])->format("Y-m-d");
        $model->load($data, '');

        $system = $model->system;
        $wactual = $model->wactual;
        $wdesire = $model->wdesire;
        $wmin = $model->wmin;
        $wmax = $model->wmax;
        $wavg = 0; //$model->wavg;
        $diet = $model->diet;
        $height = $model->height / 100;
        $age = DateHelper::age($model->dob);
        $gene = $model->gene;
        $bone = $model->bone;
        $gender = $model->gender;
        $pregnancy = $model->pregnancy;

        $imc = ($gender == self::GENDER_FEMALE) ? $height * $height * 21 : $height * $height * 24;
        $result = ((($wmin + $wmax) / 2) + $wavg + (2 * $wdesire) + (2 * $imc)) / 5;
//        return $wavg;
        if ($bone == 3) {
            $result -= 0.8;
        } else if ($bone == 1) {
            $result += 1.2;
        }

        if ($age > 30) {
            $tmp = $age - 30;
            if ($gender == self::GENDER_FEMALE) {
                $result += $tmp * 0.08;
            } else {
                $result += $tmp * 0.12;
            }
        }

        if ($gene == 2) {
            if ($gender == self::GENDER_FEMALE) {
                $result += 0.7;
            } else {
                $result += 1;
            }
        }

        if ($gender == self::GENDER_FEMALE && $pregnancy > 0) {
            $result += 0.5 * $pregnancy;
        }

//        if ($diet == 0) {
//            $result -= 0.5;
//        } else if ($diet > 2) {
//            if ($gender == self::GENDER_FEMALE) {
//                $result += 0.8;
//            } else {
//                $result += 1;
//            }
//        }

        $model->date = DateHelper::now();
        $model->result = $result;
        $model->idaccount = \Yii::$app->user->identity->idaccount;
        $model->score = self::calcScore($model);

        if($model->result - $model->wactual > 5) {
            $model->act = 3;
            $model->message = 'Vous êtes beaucoup plus mince que votre juste poids. Maigrir n\'a pas de sens et serait imprudent!';
        } else if($model->wactual <= $model->result + 2){
            $model->act = 2;
            $model->message = 'Vous êtes plus mince que votre juste poids, je vous propose la phase de consolidation qui va protéger ce poids.';
        } else {
            $model->act = 1;
            if ($model->score >= 54) {
                $model->message = 'Compte tenu de vos réponses, je vous conseille la méthode Classique, elle vous correspond parfaitement. ';
            } else {
                $model->message = 'Compte tenu de vos réponses, les deux méthodes vous conviendraient mais je vous conseillerais plutôt la Douce. Mais c’est à vous qu’appartient la décision.';
            }
        }

        $model->save();
        return $model;
    }

    public static function calcScore($model) {
        $scoreA = 0;
        $scoreB = 0;
        $scoreC = 0;
        $pA = $model->wactual - $model->result;
        $pB = $model->wmax - $model->wmin;
        $pC = $model->wdesire - $model->result;
        if ($pA < 5) {
            $scoreA = 15;
        } else if ($pA < 10) {
            $scoreA = 30;
        } else if ($pA < 15) {
            $scoreA = 45;
        } else {
            $scoreA = 60;
        }

        if ($pB < 10) {
            $scoreB = 5;
        } else if ($pB < 20) {
            $scoreB = 10;
        } else if ($pB < 30) {
            $scoreB = 15;
        } else {
            $scoreB = 20;
        }

        if ($pC < 1) {
            $scoreC = 0;
        } else if ($pC < 2) {
            $scoreC = 6;
        } else if ($pC < 3) {
            $scoreC = 7;
        } else {
            $scoreC = 7;
        }

        return $scoreA + $scoreB + $scoreC;
    }

}
