<?php

namespace app\helpers;

use Yii;

/**
 * Description of RoleHelper
 *
 * @author Eko Wahyu Wibowo <bowpunya@gmail.com>
 */
class RoleManager {
    
    
    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;
    
    const STAT_ACTIVE = 1;

    public static function isRoleUser($user, $role) {
        if ($user) {
            if (is_array($role)) {
                return in_array($user->idrole, $role);
            } else {
                return $user->idrole == $role;
            }
        }
        return false;
    }

    public static function isRole($role) {
        $user = self::identity();
        if ($user) {
            if (is_array($role)) {
                return in_array($user->idrole, $role);
            } else {
                return $user->idrole == $role;
            }
        }
        return false;
    }

    public static function isGuest() {
        return Yii::$app->user ? Yii::$app->user->isGuest : true;
    }

    public static function identity() {
        return self::isGuest() ? NULL : Yii::$app->user->identity;
    }


}
