<?php


namespace app\helpers;

class DBHelper {

public static function modelErrorsResume($model) {
        try {
            if (is_array($model)) {
                $str = '';
                foreach ($model as $row) {
                    $str .= implode(" *", $model->getFirstErrors());
                }
                return $str;
            }
            return implode(" *", $model->getFirstErrors());
        } catch (Exception $ex) {
            throw new HttpException("Tidak dapat memproses Error, mohon laporkan.");
        }
    }
}