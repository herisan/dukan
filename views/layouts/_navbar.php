<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\helpers\LinkHelper;

$session = Yii::$app->session;
if (!$session->has('lang_dukan') && !$session->has('span_language')) {
    $session->set('lang_dukan','fr');
    $session->set('span_language','CHOISISSEZ VOTRE LANGUE');
}
?>

<?php
NavBar::begin([
    'brandLabel' => Html::img('@web/img/logoheader.png',['height'=>'80px']),
    // 'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-default navbar-fixed-top',
    ],
]);?>

<span id="span-language">
    <div id="div-span-language">
        <img src="<?= Url::base()?>/img/lang/lang_<?= $session->get('lang_dukan');?>.png">
        <a href="#myModalLanguage" data-toggle="modal"> 
            <?= $session->get('span_language');?>
        </a>
    </div>
</span>
<?php
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-left',['height'=>'100px','style'=>'font-family: Roboto-Bold;color: #c9057d;']],
    'items' => [
        ['label' => 'DÉCOUVREZ NOTRE APPLI', 'url' => ['/page/find-us-app'],'active'=>LinkHelper::nav_active(Url::to(['/page/find-us-app']))],
        ['label' => 'LES APPLIS', 'url' => ['/page/apps'],'active'=>LinkHelper::nav_active(Url::to(['/page/apps']))],
        ['label' => 'LA COMMUNAUTÉ', 'url' => ['/page/community'],'active'=>LinkHelper::nav_active(Url::to(['/page/community']))],
        ['label' => 'LE BLOG', 'url' => ['/page/blog'],'active'=>LinkHelper::nav_active(Url::to(['/page/blog']))],
        ['label' => 'LA BOUTIQUE DUKAN', 'url' => ['/page/shop'],'active'=>LinkHelper::nav_active(Url::to(['/page/shop'])),'class'=>'pad-0'],
        /*Yii::$app->user->isGuest ? (
            ['label' => 'Login', 'url' => ['/site/login']]
        ) : (
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        )*/
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        ['label' => 'S\'INSCRIRE', 'url' => ['/page/sign-in'],'active'=>LinkHelper::nav_active(Url::to(['/page/sign-in']))],
        ['label' => 'CONNEXION', 'url' => ['/page/connection'],'active'=>LinkHelper::nav_active(Url::to(['/page/connection']))],
    ],
]);
NavBar::end();
?>