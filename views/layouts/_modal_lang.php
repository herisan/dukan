<?php
use yii\helpers\Url;
use app\helpers\ModuleHelper;

$session = Yii::$app->session;
?>
<div id="myModalLanguage" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

    <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" align="center"><?= $session->get('span_language');?></h4>
        </div>
        <div class="modal-body">
            <p>
                <table class="table" id="table-lang">
                    <tr>
                        <td>
                            <a href="<?= Url::to(['page/set-session-lang','lang'=>'fr'])?>">
                                <img src="<?= Url::base()?>/img/lang/lang_fr.png"><?= ModuleHelper::getLang('fr');?>
                            </a>
                        </td>
                        <td>
                            <a href="<?= Url::to(['page/set-session-lang','lang'=>'it'])?>">
                                <img src="<?= Url::base()?>/img/lang/lang_it.png"><?= ModuleHelper::getLang('it');?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="<?= Url::to(['page/set-session-lang','lang'=>'en'])?>">
                                <img src="<?= Url::base()?>/img/lang/lang_en.png"><?= ModuleHelper::getLang('en');?>
                            </a>
                        </td>
                        <td>
                            <a href="<?= Url::to(['page/set-session-lang','lang'=>'ru'])?>">
                                <img src="<?= Url::base()?>/img/lang/lang_ru.png"><?= ModuleHelper::getLang('ru');?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="<?= Url::to(['page/set-session-lang','lang'=>'es'])?>">
                                <img src="<?= Url::base()?>/img/lang/lang_es.png"><?= ModuleHelper::getLang('es');?>
                            </a>
                        </td>
                        <td>
                            <a href="<?= Url::to(['page/set-session-lang','lang'=>'tr'])?>">
                                <img src="<?= Url::base()?>/img/lang/lang_tr.png"><?= ModuleHelper::getLang('tr');?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="<?= Url::to(['page/set-session-lang','lang'=>'pr'])?>">
                                <img src="<?= Url::base()?>/img/lang/lang_pr.png"><?= ModuleHelper::getLang('pr');?>
                            </a>
                        </td>
                        <td>
                            <a href="<?= Url::to(['page/set-session-lang','lang'=>'cn'])?>">
                                <img src="<?= Url::base()?>/img/lang/lang_cn.png"><?= ModuleHelper::getLang('cn');?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="<?= Url::to(['page/set-session-lang','lang'=>'de'])?>">
                                <img src="<?= Url::base()?>/img/lang/lang_de.png"><?= ModuleHelper::getLang('de');?>
                            </a>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </p>
        </div>
        <div class="modal-footer">
        </div>
    </div>

    </div>
</div>