<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\LinkHelper;
?>
<div class="content">
    <div class="row">
        <!-- <div class="col-md-12"> -->
        <!-- <div class="col-md-12"> -->
        <div class="col-md-12">
            <div class="v1 col-md-3">
                <ul class="pad-0">&nbsp;<a href="#">SOCIÉTÉ DUKAN</a></ul>
                <ul class="pad-0">&nbsp;<a href="#">QUI SOMMES-NOUS ?</a></ul>
                <ul class="pad-0">&nbsp;<a href="#">PRESSE</a></ul>
                <ul class="pad-0">&nbsp;<a href="#">INFORMATIONS LÉGALES</a></ul>
                <ul class="pad-0">&nbsp;<a href="#">NOS PARTENAIRES</a></ul>
            </div>
            <div class="v1 col-md-3">
                <ul class="pad-0">&nbsp;<a href="#">POUR VOUS AIDER</a></ul>
                <ul class="pad-0">&nbsp;<a href="#">FAQ</a></ul>
                <ul class="pad-0">&nbsp;<a href="#">CONFIGURATION REQUISE</a></ul>
                <ul class="pad-0">&nbsp;<a href="#">PLAN DU SITE</a></ul>
            </div>
            <div class="v1 col-md-4">
                <ul class="pad-0">&nbsp;<a href="#">CHARTE DE PROTECTION DE LA VIE PRIVÉE</a></ul>
                <ul class="pad-0">&nbsp;<a href="<?= Url::to(['page/textes-bas'])?>">CONDITIONS GÉNÉRALES D'UTILISATION</a></ul>
                <ul class="pad-0">&nbsp;<a href="#">CONDITIONS GÉNÉRALES D'ABONNEMENT</a></ul>
                <ul class="pad-0">&nbsp;<a href="#">&copy GRANIT PASSION 2019</a></ul>
            </div>
            <div class="col-md-2">
                <div align="center" id="div-button-contact">
                    <?= Html::a('CONTACT',['page/contact'],['id'=>'button-contact-footer','rel'=>'nofollow noopener'])?>
                </div>

            </div>
        </div>
        <!-- </div> -->
        <!-- </div> -->
    </div>
</div>