<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use app\helpers\ModuleHelper;
use app\models\Food;

$this->title = "VOS TÉMOIGNAGES";
?>
<?php $this->registerCssFile("@web/css/page-testimoni.css")?>
<div class="content">
	<div class="row">
		<div class="col-md-12" id="page-testimoni">
			<a href="<?= Url::to(['page/'])?>" id="back2Left">
				<img src="<?= Url::base()?>/img/rowback-testimoni.png">
			</a>
			<div id="content-page-testimoni">
				<div id="title-page-testimoni">Vos témoignages</div>
				<!-- content -->
				<?php 
				$no=0;
				foreach($model as $model){$no++;?>
				<div class="col-md-12">
					<div class="col-md-5 col-float-left">
						<div class="box-testimoni">
							<div>GUL</div>
							<div>(<?= ModuleHelper::getLang($model->lang);?>)</div>
							<div><?= Food::getMethodShort($model->method);?></div>
							<div><?= $model->age;?> ans</div>
						</div>
						<div class="image-testimoni">
							<img src="<?= Url::base()?>/img/testimoni/<?= $model->image;?>" align="left">
						</div>
					    <div class="font3-right-testimoni" align="right">
						    <div class="rcorners2" align="center">
					    		<div>
					    		<div>
						    	<span class="plus-font3-right-testimoni">
						    		+
						    	</span>
				    			<span class="number-font3-right-testimoni">
				    				<?= (int)$model->wstart - (int)$model->wlast;?>
				    			</span>
				    			<div class="div-font3-right-testimoni">
				    				<span class="span-div-font3-right-testimoni">
					    			<span class="span-font3-right-testimoni">kg</span>
					    			</span>
				    			</div>
				    			</div>
				    			</div>
				    			<div class="perdus-font3-right-testimoni">perdus</div>
						    </div>
					    </div>
					</div>
					<div class="col-md-7 col-float-right">
						<div class="text-testimoni">
						<?= $model->desc;?>
						</div>
						<div class="link-plus">
							<?= Html::a('Plus',['/page/testimoni-readmore','id'=>$model->idtestimoni])?>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="col-md-12 div-line"></div>
					</div>
				</div>
				<?php 
					}
				?>
			<?php 
			echo LinkPager::widget([
			    'pagination' => $pagination,
			    'firstPageLabel'=>'&lArr;',
			    'lastPageLabel'=>'&rArr;',
			]);
			?>
			</div>
			<?php if($no>1){?>
			<div class="col-md-12" id="div-back2Top">
				<a href="#" id="back2Top">
					<img src="<?= Url::base()?>/img/rowup-testimoni.png">
				</a>
			</div>
			<?php }?>
		</div>
	</div>
</div>