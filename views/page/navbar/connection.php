<?php
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = "CONNEXION";
?>
<style type="text/css">
	div#div-connection{
		background: linear-gradient(#01c9ed,#395b9a);
		font-family: Rondalo;
	}
	div#div-connection,div#div-connection-button{
		color: #ffffff;
	}
	div#div-connection-button{
		background: #c8057d;
	}
	a.conn-link{
		color: #ffffff;
		text-decoration: none;
	}
</style>
<div class="content">
	<div class="row">
		<div class="col-md-12" align="center" style="padding: 0">
			<div class="col-md-12" id="div-connection">
				<div class="col-md-12">
					<div style="font-size: 75px;">Régime Dukan</div>
					<div style="font-size: 30px;">L'Officiel PREMIUM</div>
				</div>
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<div style="border: solid 0.5px #ffffff;border-radius:5px;background-color: #395b9a;font-family: Roboto-Bold;">
						<a href="#" class="conn-link">CONNEXION AVEC FACEBOOK <i class="fa fa-facebook-official"></i></a>
					</div>
				</div>
				<div class="col-md-4"></div>
				<div class="col-md-12">OU</div>
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<div style="border: solid 0.5px #ffffff;border-radius:5px;background-color: #0775bc;font-family: PTS75F;">
						<div>Entrez votre n de telephone</div>
						<div>ou votre email pour vous connecter</div>
					</div>
				</div>
				<div class="col-md-4"></div>
				<div class="col-md-12"><br></div>
				<div class="col-md-5"></div>
				<div class="col-md-2">
					<div style="border-radius:5px;background-color: #c8057d;font-family: PTS75F;">
						<?= Html::a('CONNEXION',['page/connection-email'],['class'=>'conn-link'])?>
					</div>
				</div>
				<div class="col-md-5"></div>
				<div class="col-md-12">
					<img src="<?= Url::base()?>/img/g963.png">
				</div>
			</div>
			<div class="col-md-12" id="div-connection-button">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<div style="border: solid 3px #ffffff;margin: 30px;font-family: PTS75F;padding: 5px;">
						<a href="#" class="conn-link">Si vous n'êtes pas enregistré CRÉER VOTRE COMPTE</a>
					</div>

				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
	</div>
</div>