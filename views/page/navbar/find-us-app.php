<?php
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = "DÉCOUVREZ NOTRE APPLI";
?>
<?php $this->registerCssFile("@web/css/carousel.css")?>
<div class="content">
	<div class="row">

		<!-- thumbnail slider -->
		<div class="col-md-12" align="center" style="padding: 0">
		  	<div id="carousel-custom" class="carousel slide" data-ride="carousel">
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
					  <img src="<?= Url::base()?>/img/slider/slide1.png" alt="..." class="img-responsive" style="height: 500px">
					</div>
					<div class="item">
					  <img src="<?= Url::base()?>/img/slider/slide2.png" alt="..." class="img-responsive" style="height: 500px">
					</div>
					<div class="item">
					  <img src="<?= Url::base()?>/img/slider/slide3.png" alt="..." class="img-responsive" style="height: 500px">
					</div>
					<div class="item">
					  <img src="<?= Url::base()?>/img/slider/slide4.png" alt="..." class="img-responsive" style="height: 500px">
					</div>
					<div class="item">
					  <img src="<?= Url::base()?>/img/slider/slide5.jpg" alt="..." class="img-responsive" style="height: 500px">
					</div>
					<div class="item">
					  <img src="<?= Url::base()?>/img/slider/slide6.jpg" alt="..." class="img-responsive" style="height: 500px">
					</div>
					<div class="item">
					  <img src="<?= Url::base()?>/img/slider/slide7.jpg" alt="..." class="img-responsive" style="height: 500px">
					</div>
					<div class="item">
					  <img src="<?= Url::base()?>/img/slider/slide8.jpg" alt="..." class="img-responsive" style="height: 500px">
					</div>
					<div class="item">
					  <img src="<?= Url::base()?>/img/slider/slide9.jpg" alt="..." class="img-responsive" style="height: 500px">
					</div>
					<div class="item">
					  <img src="<?= Url::base()?>/img/slider/slide10.jpg" alt="..." class="img-responsive" style="height: 500px">
					</div>
					<div class="item">
					  <img src="<?= Url::base()?>/img/slider/slide11.jpg" alt="..." class="img-responsive" style="height: 500px">
					</div>
					<div class="item">
					  <img src="<?= Url::base()?>/img/slider/slide12.jpg" alt="..." class="img-responsive" style="height: 500px">
					</div>
				</div>

				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-custom" role="button" data-slide="prev">
					<i class="glyphicon glyphicon-menu-left"></i>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel-custom" role="button" data-slide="next">
					<i class="glyphicon glyphicon-menu-right"></i>
					<span class="sr-only">Next</span>
				</a>
			  	<div class="col-md-12" align="center" style="text-align: center;">
		  		<!-- Indicators -->
				  <ol class="carousel-indicators visible-sm-block hidden-xs-block visible-md-block visible-lg-block">
		            <li data-target="#carousel-custom" data-slide-to="0" class="active">
		              <img src="<?= Url::base()?>/img/slider/slide1.png" alt="..." class="img-responsive" style="width: 100px">
		            </li>
		            <li data-target="#carousel-custom" data-slide-to="1">
		              <img src="<?= Url::base()?>/img/slider/slide2.png" alt="..." class="img-responsive" style="width: 100px">
		            </li>
		            <li data-target="#carousel-custom" data-slide-to="2">
		              <img src="<?= Url::base()?>/img/slider/slide3.png" alt="..." class="img-responsive" style="width: 100px">
		            </li>
		            <li data-target="#carousel-custom" data-slide-to="3">
		              <img src="<?= Url::base()?>/img/slider/slide4.png" alt="..." class="img-responsive" style="width: 100px">
		            </li>
		            <li data-target="#carousel-custom" data-slide-to="4">
		              <img src="<?= Url::base()?>/img/slider/slide5.jpg" alt="..." class="img-responsive" style="width: 100px">
		            </li>
		            <li data-target="#carousel-custom" data-slide-to="5">
		              <img src="<?= Url::base()?>/img/slider/slide6.jpg" alt="..." class="img-responsive" style="width: 100px">
		            </li>
		            <li data-target="#carousel-custom" data-slide-to="6">
		              <img src="<?= Url::base()?>/img/slider/slide7.jpg" alt="..." class="img-responsive" style="width: 100px">
		            </li>
		            <li data-target="#carousel-custom" data-slide-to="7">
		              <img src="<?= Url::base()?>/img/slider/slide8.jpg" alt="..." class="img-responsive" style="width: 100px">
		            </li>
		            <li data-target="#carousel-custom" data-slide-to="8">
		              <img src="<?= Url::base()?>/img/slider/slide9.jpg" alt="..." class="img-responsive" style="width: 100px">
		            </li>
		            <li data-target="#carousel-custom" data-slide-to="9">
		              <img src="<?= Url::base()?>/img/slider/slide10.jpg" alt="..." class="img-responsive" style="width: 100px">
		            </li>
		            <li data-target="#carousel-custom" data-slide-to="10">
		              <img src="<?= Url::base()?>/img/slider/slide11.jpg" alt="..." class="img-responsive" style="width: 100px">
		            </li>
		            <li data-target="#carousel-custom" data-slide-to="11">
		              <img src="<?= Url::base()?>/img/slider/slide12.jpg" alt="..." class="img-responsive" style="width: 100px">
		            </li>

		          </ol> 
				</div>
		  	</div>
		  
	  	</div>
	  	<!-- end carousel-->
	</div>
</div>
