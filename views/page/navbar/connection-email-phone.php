<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "CONNEXION";
?>
<style type="text/css">
	div#div-connection{
		background: linear-gradient(#01c9ed,#395b9a);
		font-family: Rondalo;
	}
	div#div-connection,div#div-connection-button{
		color: #ffffff;
	}
	div#div-connection-button{
		background: #c8057d;
	}
	a.conn-link{
		color: #ffffff;
		text-decoration: none;
	}
</style>
<div class="content">
	<div class="row">
		<div class="col-md-12" align="center" style="padding: 0">
            <?php $form = ActiveForm::begin(['id'=>'dukan-login-form']) ?>

			<div class="col-md-12" id="div-connection">
				<div class="col-md-12">
					<div style="font-size: 75px;">Régime Dukan</div>
					<div style="font-size: 30px;">L'Officiel PREMIUM</div>
				</div>
				<div class="col-md-12">
					<img src="<?= Url::base()?>/img/g963.png">
				</div>
				<div class="col-md-5"></div>
				<div class="col-md-2">
					<?= $form->field($model, 'username')->textInput(['placeholder'=>'Identifier'])->label(false) ?>
					<?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Mot de passe'])->label(false) ?>
				</div>
				<div class="col-md-5"></div>
			</div>
			<div class="col-md-12" id="div-connection-button">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<div style="border: solid 3px #ffffff;margin: 30px;font-family: PTS75F;padding: 5px;">
						<a href="#" class="conn-link" onclick="document.getElementById('dukan-login-form').submit();">CONNEXION</a>
					</div>

				</div>
				<div class="col-md-4"></div>
			</div>

            <?php ActiveForm::end() ?>
		</div>
	</div>
</div>