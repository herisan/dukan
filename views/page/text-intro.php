<?php
use yii\helpers\Url;

$this->title = "L'APPLICATION EVENEMENT";
?>

<?php $this->registerCssFile("@web/css/page-text-intro.css")?>
<div class="content">
	<div class="row">
		<div class="col-md-12" id="div-pargh-text-intro">
			<a href="<?= Yii::$app->homeUrl;?>" id="back2Left">
				<img src="<?= Url::base()?>/img/rowback.png">
			</a>
			<div class="col-md-2"></div>
			<div id="pargh" class="col-md-8" style="">
				<p>Cette application officielle Dukan fait suite à une application précédente qui a eu beaucoup de 
				succès dans de très nombreux pays du monde.</p>
				<p>Il existe de très nombreuses applications de suivi de régime. La grande majorité de ces applications permettent à l’utilisateur de suivre sa courbe de poids, ses calories consommées et brulées par l’acti-vité physique, de choisir lui-même ses plats et ses recettes.</p>
				<p>Cette application s’en distingue car elle ne fonctionne pas en libre-service où chacun peut choisir sa voie en se contentant de fixer lui-même sa consommation calorique. Si c’est ce que vous cherchez, choisissez une autre application.</p>
				<p>L’application officielle Dukan ne vous propose qu’un seul et unique choix : maigrir avec l’une des 
				deux méthodes Dukan, la forte ou la douce en fonction de votre profil psychologique et nutritionnel. 
				Ces deux méthodes ont été conçues et composée par le Dr Dukan à l’usage de personnes en surpo-
				ids très motivées et cherchant à maigrir aussi vite que possible et en toute sécurité. 17 millions de ses livres ont été vendus dans plus de 30 pays dont les Etats Unis où The Dukan Diet fut pendant 6 se-
				maines en #1 bestseller du New York Time.</p>
				<p>La caractéristique première de ces deux méthodes est de refuser le dogme des calories au profit des catégories, éliminant les carbs au profit des protéines, des légumes et des graisses essentielles 
				de poissons. Ce qui lui vaut d’être considérée comme l’une des plus efficaces au monde. C’est aussi
				la plus simple et la plus structurée.</p>
				<p>
					Sa structure tient à sa composition en 4 phases : deux pour maigrir, l’Attaque courte et foudroyante et la Croisière qui ralentit la cadence pour conduire au Juste Poids Et deux autres phase pour éviter les si fréquentes reprises de poids : la Consolidation pour éviter le premier raz-de-marée déclenché par l’arrêt du régime et la stabilisation définitive qui devient un mode de vie nouveau avec une éli-mination progressive des habitudes ayant conduit au surpoids.
				</p>
				<p>Elle propose 100 aliments et à volonté, ce qui évite la faim et le comptage des calories.
				Dans cette application, vous trouverez tout ce que les technologies les plus récentes utilisent pour 
				suivre les méthodes Dukan de façon intuitive. L’accompagnement est directif, évolue en fonction 
				des résultats. Il prend l’utilisateur du 1er jour de la phase d’attaque pour ne plus le lâcher.</p>
				<p>Ses principaux atouts sont :</p>
				<p>Le Calcul du Juste Poids qui est une exclusivité crée par le Dr Dukan et qui déjà été utilisée par 9 mil-lions de personnes dans le monde, un poids personnel calculé à partir de 11 paramètres et permet-tant de fixer pour chaque utilisateur le poids le moins frustrant à atteindre et qu’il a les meilleures chances de conserver.</p>
				<p>Le choix entre la méthode Classique – la Forte – et la Méthode de l’Escalier Nutritionnel – la Douce – avec choix conseillé en fonction du profil (poids à perdre, niveau de motivation, vie sociale, urgence de maigrir…).</p>
				<p>La Courbe pondérale avec affichage des écarts par repère de couleur.</p>
				<p>Le suivi en boucle quotidien avec consignes du matin, alimentaires et d’activité physiques et compte rendu simplifié le soir avec comptage automatique des pas et des boissons.</p>
				<p>Le Bilan du jour avec un note de synthèse décomposée en fonction des éventuels écarts de régime, 
				du nombre de pas et des exercices prescrits, de l’eau bue, du son d’avoine consommé.</p>
				<p>Ecran de pilotage avec affichage graphique de l’ensemble de paramètres de suivi de la Méthode 
				: Courbe du poids avec affichage des écarts, Courbe de l’activité physique, Courbe de la motivation 
				et des émotions.</p>
				<p>Le contrôle des écarts avec les mesures de correction immédiate.</p>
				<p>La bibliothèque des recettes avec moteur de recherche selon l’ingrédient désiré Bibliothèque des recettes favorites et des recettes créées et proposées à la communauté.</p>
				<p>La Boussole Emotionnelle avec soutien de motivation et de l’humeur. Un éventail de 20 émotions 
				positives et négatives permettant de sélectionner celles rencontrées au cours de la journée.</p>
				<p>Le Joker hebdomadaire, une autre exclusivité. A la question « Quel est l’aliment qui vous a le plus manqué aujourd’hui ? Une réponse indiquant avec insistance le même aliment déclenche la propo-
				sition d’un Joker. Exemple, le manque répété des fruits propose comme diner un plateau de 800 g de fruits avec 2 yaourts maigres.</p>
				<p>Le Bouton SOS à utiliser en cas de difficultés, d’inquiétude, de préoccupation, de questionnement 
				stressant. Il déclenche une assistance permanente avec réponses à visage humain du coach par chat et, en cas de question plus pointue, par le Dr Dukan lui-même.</p>
				<p>Un bouton FAQ pour répondre aux questions les plus simples et usuelles.</p>
				<p>La prise en charge de la stagnation avec la technique des Ripostes graduées en fonction de sa 
				durée.</p>
				<p>Ma Santé m’intéresse : une information sur les points de vulnérabilité de l’utilisateur comme le dia-bète, la thyroïde, la santé cardiovasculaire, le cholestérol, le sommeil, la dépression.</p>
				<p>La traque des mauvaises habitudes et leur inversion en positives à partir de la Consolidation.</p>
				<p>La Parole quotidienne du Dr Dukan sur un thème différent chaque jour délivrant une information 
				fiable facilitant la prise en charge du surpoids.</p>
				<p>La Silhouette évolutive, une autre exclusivité qui permet de suivre au centimètre près l’évolution du corps en fonction du poids perdu, sur l’espace Taille/Hanches/Cuisses.</p>
				<p>Les Dix Piliers du Bonheur – exclusivité – qui permet de sonder la solidité et la satisfaction des Dix grands besoins fondamentaux : la bouffe, l’amour, le pouvoir, le corps, le besoin de nature, l’habitat, l’appartenance au groupe, le jeu, le besoin du sacré et celui du beau. Sachant que l’insuffisance de satisfaction de ces besoins cherche compensation vers les deux plus simples à satisfaire, la bouffe et le jeu (les écrans).</p>
				<p>La liste des aliments autorisés à volonté et la liste des Tolérés à raison de deux par jour dès la 
				phase d’attaque achevée, sauf en stagnation.</p>
				<p>La possibilité de passerelle à deux sens entre la Méthode Classique et l’Escalier Nutritionnel.
				Enfin, la période d’essai gratuite de trois jours pour chacune des deux méthodes avec possibilité de 
				tester les deux successivement avec l’intégralité des services.</p>
				<p>Le prix de la totalité de l’amaigrissement incluant la phase d’attaque et de croisière pour la mé-
				thode Classique et celle de l’Escalier nutritionnel est de 20 euros, quel que soit le poids à perdre.</p>
				<p>En phase de consolidation, le prix est mensuel et de 5€ par mois.</p>
				<p>En phase de stabilisation, le prix est mensuel et de 3€ par mois.</p>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-12" id="p-back2Top">
				<p>
					<a href="#" id="back2Top">
						<img src="<?= Url::base()?>/img/rowup.png">
					</a>
				</p>
			</div>
		</div>
	</div>
</div>