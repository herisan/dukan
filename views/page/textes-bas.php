<?php
use yii\helpers\Url;

$this->title = "CONDITIONS GÉNÉRALES D'UTILISATION";
?>

<?php $this->registerCssFile("@web/css/page-textes-bas.css")?>
<div class="content">
	<div class="row">
		<div class="col-md-12" id="div-pargh-text-intro" style="background: #f2f3f5">
			<!-- <div class="col-md-12"> -->
				<!-- <div> -->
					<a href="<?= Yii::$app->homeUrl;?>" id="back2Left">
						<img src="<?= Url::base()?>/img/rowback-textes-bas.png">
					</a>
				<!-- </div> -->
			<!-- </div> -->
			<div class="col-md-2"></div>
			<div id="pargh" class="col-md-8" style="color: #4b4b4d;">
					<p>
						<div style="color: #4b4b4d;font-size: 40px;text-align: center;margin-bottom: 40px">Nos Conditions Générales d'Utilisation</div>
					</p>
				<p>VEUILLEZ LIRE ATTENTIVEMENT LES PRESENTES CONDITIONS GENERALES D'UTILISATION QUI RE-GISSENT VOTRE UTILISATION DE NOTRE SITE WEB</p>
				<p>Votre utilisation de notre Site Web vaut acceptation des présentes Conditions Générales d’Utilisa-
				tion Pour vous abonner ou devenir membre de la communauté des utilisateurs, vous serez invi-
				té(e) à lire les Conditions Générales d'Utilisation et à les accepter. Si vous ne faites que visiter le Site
				Web, vous ne serez pas obligé(e) d’accepter de manière expresse les Conditions Générales d'Utili-
				sation en cochant la case prévue à cet effet mais votre utilisation du Site Web vaudra acceptation
				desdites Conditions Générales d'Utilisation. Si vous ne souhaitez pas être lié(e) par toutes ou partie
				des dispositions des Conditions Générales d'Utilisation, veuillez ne pas utiliser le Site Web ni cocher
				la case pour signifier votre acceptation. L’utilisation par vous de tout ou partie du Site Web ou l’ac-
				ceptation expresse par vous des Conditions Générales d'Utilisation en cochant la case prévue à cet
				effet lorsque cela vous est demandé signifie votre acceptation des présentes Conditions Générales
				d’Utilisation. Nous nous réservons le droit, à tout moment et à notre entière discrétion, de changer,
				modifier, compléter ou supprimer certaines dispositions des Conditions Générales d'Utilisation. Il
				vous appartient de consulter régulièrement les présentes Conditions Générales d'Utilisation afin de
				prendre connaissance des changements qui y sont apportés. Nous vous informerons des change-
				ments apportés aux présentes Conditions Générales d'Utilisation en les affichant sur le Site Web. En
				cas de changements importants apportés aux présentes Conditions Générales d'Utilisation, vous en
				serez informé(e) par tout moyen que nous considérerons comme approprié. L’utilisation du présent
				Site Web après la publication de changements relatifs aux Conditions Générales d'Utilisation vaut
				acceptation desdits changements. Si vous n’acceptez pas les présentes Conditions Générales d'Utili-
				sation et/ou leurs modifications, vous ne devez pas utiliser notre Site Web et, le cas échéant, vous
				devez résilier votre (vos) compte(s) utilisateur de la communauté ou votre abonnement.</p>
				<p style="margin-bottom: 0;color: #007174">1. Portée des Conditions Générales d'Utilisation</p>
				<p>Sauf disposition contraire, les présentes Conditions Générales d'Utilisation s’appliquent à votre utili-sation des sites web français qui appartiennent ou qui sont exploités par DUKAN et nos Affiliées
				(ci-après collectivement, « nous », « nos » ou « notre »), y compris, notamment, le présent site Web
				et tout autre site Web en français que nous pourrions posséder ou exploiter aujourd’hui ou ultérieu-
				rement (ci-après collectivement notre « Site Web »).</p>
				<p>Aux fins des présentes Conditions Générales d'Utilisation, « Affiliées » désigne toute personne, phy-sique ou morale, détenant directement ou indirectement une participation majoritaire dans, déte-
				nue par ou sous le même contrôle que DUKAN. Le présent Site Web et les Conditions Générales
				d'Utilisation s’adressent spécifiquement aux habitants de France Métropolitaine ou DROM et le
				Site Web ne doit être utilisé que par eux.</p>
				<p style="margin-bottom: 0;color: #007174">2. Conditions générales d'Utilisation – Généralités</p>
				<p>En utilisant le présent Site Web, vous acceptez d’être légalement lié(e) par les Conditions Générales d'Utilisation et de les respecter, de la même manière que si vous aviez signé un contrat. Si à un moment quelconque, vous ne respectez pas les Articles 3, 6 et 11 des présentes Conditions Générales d'Utilisation, nous nous réservons le droit, le cas échéant, de désactiver automatiquement et de plein droit, votre mot de passe, votre compte utilisateur et/ou de bloquer votre accès au Site Web (totalement ou partiellement) moyennant un préavis raisonnable. Toutefois, en cas de manque-
				ment grave à l’Article 5 des présentes Conditions Générales d'Utilisation, sauf disposition contraire
				éventuelle, vous reconnaissez que nous pourrons immédiatement et sans préavis mettre un terme
				à votre accès ou à votre utilisation du Site Web en désactivant ou en supprimant votre compte utili-
				sateur ainsi que les informations et fichiers connexes figurant dans votre compte utilisateur et/ou in- terdire tout accès ultérieur à ces informations et/ou fichiers ainsi qu’à notre Site Web. De plus, vous reconnaissez que nous ne serons pas responsables à votre égard ou à l’égard des tiers des dom- mages pouvant résulter de la résiliation ou du blocage de l’accès ou de l’utilisation de notre Site Web suite à un manquement de votre part aux dispositions ci-dessus. Nous pourrons régulièrement
				compléter les présentes Conditions Générales d'Utilisation par de nouvelles dispositions relatives à
				un contenu, à des activités ou à des événements particuliers (ci-après les « Conditions Générales
				d'Utilisation Supplémentaires »). Ces Conditions Générales d'Utilisation Supplémentaires peuvent
				être publiées sur le Site Web afin d’être visualisées dans le cadre du contenu, des activités, des fonc- tionnalités ou des événements particuliers et seront identifiées en tant que telles. Il pourra vous être demandé d’accepter ces conditions avant d’utiliser le contenu, les activités, les fonctionnalités, les promotions ou les événements en question. Vous comprenez et acceptez que ces Conditions Géné- rales d'Utilisation Supplémentaires sont, par les présentes, intégrées par référence aux Conditions Générales d'Utilisation. A notre entière discrétion, avec ou sans préavis et sans encourir une quel- conque responsabilité, nous pourrons interrompre, modifier ou altérer tout aspect du Site Web, y compris, notamment (i) réduire la disponibilité du Site Web, et (ii) limiter l’étendue de l’utilisation autorisée.</p>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-12" id="p-back2Top">
				<p>
					<a href="#" id="back2Top">
						<img src="<?= Url::base()?>/img/rowup-textes-bas.png">
					</a>
				</p>
			</div>
		</div>
	</div>
</div>