<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "HOME";
?>
<?php $this->registerCssFile("@web/css/page-index.css")?>
<div class="">
	<div class="row">
		<div class="col-md-12" id="page-intro">
		<div class="col-md-6" id="text-intro">
			<div id="sub-text-intro">
				<p class="p-text-intro">L'APPLICATION EVENEMENT</p>

				<p class="p-text-intro">En 2015, j'ai  créé  l'Application  Dukan Officielle pour encadrer et faciliter le suivi de la méthode classique. Cette application a été downloadée plus d'un milion de fois dans le monde.</p>

				<p class="p-text-intro-2">Aujourd'hui,  voici  l'Ultime  application  : la PREMIUM.<br>
				C'est à ma connaissance l'application du maigrir la plus performante au monde.<br>
				Elle posséde TOUT ce qui peut conduire à votre réussite.<br>
				Le clou de l'application est la mise en relation DIRECTE avec moi, si necessaire.</p>
				<div id="div-button-readmore" align="center">
					<?= Html::a('EN SAVOIR PLUS',['page/readmore-text-intro'],['id'=>'button-text-intro','rel'=>'nofollow noopener'])?>
				</div>
			</div>
		</div>
		<div class="col-md-6" id="right-text-intro">
			<div align="right" id="rcorners1">
				<div id="sub-rcorners1">
					<div id="div1-sub-rcorners1" class="font-rcorners1-1 font-size-70">Maigrir</div>
		    		<div id="div2-sub-rcorners1" class="font-rcorners1-2 font-size-40">bien, vite et en</div>
		    		<div id="div3-sub-rcorners1" class="font-rcorners1-1 font-size-70" id="mrg1">bonne santé </div>
		    		<div id="div4-sub-rcorners1" class="font-rcorners1-2 font-size-40">avec mes 2 méthodes</div>
		    		<div id="img-sub-rcorners1">
		    			<img src="<?= Url::base()?>/img/g13000.png">
		    		</div>
				</div>
			</div>
		    <img src="<?= Url::base()?>/img/g4837.png" align="right" id="image-text-intro">
		</div>
		</div>
	</div>
	<div class="row" id="row2">
		<div class="col-md-12" id="page-testimoni">
			<div class="col-md-6" id="left-testimoni">
				<div align="center">
				    <div align="center" id="rcorners2">
				    		<div id="number-left-testimoni">
				    			5
				    		</div>
				    		<div id="font-left-testimoni" align="center">
					    		<span id="sub-font-left-testimoni">€</span>/mois
					    	</div>
				    </div>
			    </div>
			    <div id="font-left-testimoni-2">
				    <div>
				    	<div id="div1-font-left-testimoni-2" class="font-size-40">Pour atteindre </div>
				    	<div id="div2-font-left-testimoni-2" class="font-size-40">votre <span class="font-size-70">juste poids </span>!</div>
				    </div>
				    <div id="div-img-font-left-testimoni-2"><img src="<?= Url::base()?>/img/g13001.png"></div>
			    </div>
			    <div align="center">
					<?= Html::a('CALCULEZ VOTRE JUSTE POIDS',['page/readmore-text-intro'],['id'=>'button-left-testimoni','rel'=>'nofollow noopener'])?>
				</div>
			</div>
			<div class="col-md-6" id="right-testimoni">
				<div id="font-right-testimoni" align="center">
			    	<span id="span1-font-right-testimoni" align="center">+500</span>
			    	<span id="span2-font-right-testimoni" align="center">témoignages</span>
			    </div>
			    <div id="img-right-testimoni" align="center">
			    	<img src="<?= Url::base()?>/img/g4583.png" width="300" height="300">
			    </div>
			    <div id="font2-right-testimoni" align="left">
			    	<div>
			    		<span>GUL 
			    			<span>(Turquie)</span>
			    		</span>
			    	</div>
			    </div>
			    <div id="font3-right-testimoni" align="right">
				    <div align="right" id="rcorners2">
				    	<div>
				    		<div>
				    			<span id="plus-font3-right-testimoni">+</span>
				    			<span id="number-font3-right-testimoni">
				    				50
				    			</span>
				    			<div id="div-font3-right-testimoni">
				    				<span id="span-div-font3-right-testimoni">
					    				kg
					    			</span>
				    			</div>
				    		</div>
			    			<div id="perdus-font3-right-testimoni">perdus</div>
				    	</div>
				    </div>
			    </div>
			    <div align="center">
			    	<?= Html::a('VOIR LES AUTRES',['page/testimoni'],['id'=>'button-right-testimoni','rel'=>'nofollow noopener'])?>
			    </div>
			    <div id="sub-div-right-testimoni" align="center">
			    	<a id="a-sub-div-right-testimoni" href="add-website-here" rel="nofollow noopener">JE COMMENCE</a>
			    </div>
		    </div>
		</div>
	</div>
	<div class="row" id="curva-dukan">
		<div class="col-md-12" id="div1-curva-dukan">
		<div class="col-md-5">
			<div>
		    	<img id="img-curva-dukan" src="img/g1168.png">
		    </div>
		</div>
		<div id="div3-curva-dukan" class="col-md-7">
			<p><font id="font-p-div3-curva-dukan">Avec votre
				<font id="font2-p-div3-curva-dukan"> écran de pilotage</font>
		    	<br>je suis à vos côtés tout au long de la journée!
		    	</font>
		    </p>
		    <br>
		    <div class="sub-div3-curva-dukan"><font class="font-sub3-div-curva-dukan">Ma courbe de poids, au jour le jour</font></div>
		    <div class="sub-div3-curva-dukan"><font class="font-sub3-div-curva-dukan">Mon activité physique</font></div>
		    <div class="sub-div3-curva-dukan"><font class="font-sub3-div-curva-dukan">Ma consommation d'eau, de son d'avoine...</font></div>
		    <div class="sub-div3-curva-dukan"><font class="font-sub3-div-curva-dukan">Accés direct à mon journal, mes repas, mes recettes, mes aliments autorisés et tolérés</font></div>
		</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="row-premium">
		<div class="col-md-6" id="left-premium-vers">
			<div id="sub1-left-premium-vers">
			    <div id="sub1-sub1-left-premium-vers">
			    	<span>Les exclusivités de </span>
			    </div>
		    	<div id="sub2-sub1-left-premium-vers">
		    		<span id="span-sub2-left-premium">la version 
		    			<span id="sub-span-sub2-left-premium">PREMIUM</span>!</span>
		    	</div>
			    <div id="sub3-sub1-left-premium-vers">
			    	<ul id="bulletblue">
				    	<li class="li-bulletblue-sub3-sub1-left-premium-vers">
				    		<div class="div-li-bulletblue-sub3-sub1-left-premium-vers">
				    			<font>Mes 2 méthodes au choix, la classique OU I'escalier nutritionnel ET ma recommandaition</font>
				    			</div>
				    	</li>
				    	<li class="li-bulletblue-sub3-sub1-left-premium-vers">
				    		<div id="div1-li-bulletblue-sub3-sub1-left-premium-vers" class="div-li-bulletblue-sub3-sub1-left-premium-vers">
					    		Ne plus regrossir... les 3 fonctions anti-yoyo:
					    	</div>
				    		<div id="div2-li-bulletblue-sub3-sub1-left-premium-vers" class="div-li-bulletblue-sub3-sub1-left-premium-vers">
					    		<img src="<?= Url::base()?>/img/text1510.png">
					    	</div>
				    		<div id="div3-li-bulletblue-sub3-sub1-left-premium-vers" class="div-li-bulletblue-sub3-sub1-left-premium-vers">
				    			<ul>
				    			<table id="table-left-premium-vers">
				    				<tr>
				    					<td>
				    						<font class="font-sans-serif-table-left-premium-vers">1)</font>
				    						Une riposte anti reprise de poids
				    					</td>
				    					<td>
				    						<a id="button1-left-premium-vers" class="button-left-premium-vers" href="add-website-here" target="_blank" rel="nofollow noopener">EN SAVOIR PLUS</a>
				    					</td>
				    				</tr>
				    				<tr>
				    					<td>
				    						<font class="font-sans-serif-table-left-premium-vers">2)</font>
				    						Elimination de vos mauvaises habitudes
				    					</td>
				    					<td><a id="button2-left-premium-vers" class="button-left-premium-vers" href="add-website-here" target="_blank" rel="nofollow noopener">EN SAVOIR PLUS</a>
				    					</td>
				    				</tr>
				    				<tr>
				    					<td>
				    						<font class="font-sans-serif-table-left-premium-vers">3)</font>
				    						Créer du bonheur pour moins manger
				    					</td>
				    					<td><a id="button3-left-premium-vers" class="button-left-premium-vers" href="add-website-here" target="_blank" rel="nofollow noopener">EN SAVOIR PLUS</a></td>
				    				</tr>
				    			</table>
					    		</ul>
				    		</div>
				    	</li>
				    	<li class="li-bulletblue-sub3-sub1-left-premium-vers">
				    		<div class="div-li-bulletblue-sub3-sub1-left-premium-vers">En 8 langues</div>
				    	</li>
				    	<li id="last-li-bulletblue">
				    		<div>
				    			Balance et montre interconnectees
				    		</div>
				    	</li>
				    </ul>
				</div>
		    </div>
		</div>
		<div class="col-md-6" id="right-premium-vers"style="">
			<img src="<?= Url::base()?>/img/g14.png">
			<div id="div1-right-premium-vers">
		    	<p>En PREMIUM, <br> commande en ligne <br>de produits Dukan <br> et livraison à domicile <br> et...</p>
	    	</div>
	    	<div id="div2-right-premium-vers">
		    	<div>
		    		<font id="font1-div2-right-premium-vers"> 10% </font>
		    		<font id="font2-div2-right-premium-vers">de remise</font>
		    	</div>
		    </div>
		    <div id="div3-right-premium-vers" class="button_cont" align="center">
		    	<a class="example_c" href="add-website-here" target="_blank" rel="nofollow noopener">LA BOUTIQUE</a>
		    </div>
		</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="page-sos">
		<div class="col-md-6" id="left-sos">
			<img src="<?= Url::base()?>/img/mobile.png">
		</div>
		<div class="col-md-6" id="right-sos">
			<div align="right">
		    	<a id="button-right-sos" href="add-website-here" rel="nofollow noopener">
		    		<span id="span1-button-right-sos">Bouton</span>&nbsp;
		    		<span id="span2-button-right-sos">SOS</span> 
		    	</a>
		    	
		    </div>
		    <div id="div2-right-sos">
		    	<font class="font-size-60 color-0e787d">Mon coach</font>
		    </div>
		    <div id="div3-right-sos">
		    	<font class="font-size-30 color-0e787d">En direct</font>
		    </div>
		    <div>
		    	<!-- <img src="<?= Url::base()?>/web/img/g1598.png" style="width: 150px;margin-left: -60px"> -->
		    </div>
		    <div id="div5-right-sos">
		    	<font class="font-size-60 color-c9057d">Pierre Dukan</font>
		    </div>
		    <div id="div6-right-sos">
		    	<font class="font-size-30 color-c9057d">En cas de difficultés</font>
		    </div>
		    <div id="div7-right-sos">
		    	<font class="font-size-30 color-c9057d">avec le Dr Dukan 
		    		<font class="font-size-40 color-c9057d">lui-même</font> !
		    	</font>
		    </div>
		    <div id="div8-right-sos">
		    	<img src="<?= Url::base()?>/img/g2159.png">
		    </div>
		</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="img-sync">
			<p>
		    	<span id="span1-img-sync">Ma nouvelle appli,</span>
		    	<span id="span2-img-sync"> tous les jours avec vous!</span>
		    </p>
		    <div align="center">
		    	<img src="<?= Url::base()?>/img/g1655.png">
		    </div>
		</div>
	</div>
</div>