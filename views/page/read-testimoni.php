<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\helpers\ModuleHelper;
use app\models\Food;

$this->title = "TÉMOIGNAGES";
?>
<?php $this->registerCssFile("@web/css/page-read-testimoni.css")?>
<div class="content">
	<div class="row">
		<div class="col-md-12" id="page-testimoni">
			<div id="page-header">
				<a href="<?= Url::to(['page/testimoni'])?>" id="back2Left">
					<img src="<?= Url::base()?>/img/rowback-testimoni.png">
				</a>
				<div id="title-page-testimoni">Témoignages</div>
			</div>
			<div id="content-page-testimoni">
				<!-- content -->
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="col-md-3" align="center">
							<div class="box-testimoni" align="center">
								<div>GUL</div>
								<div>(<?= ModuleHelper::getLang($model->lang);?>)</div>
								<div><?= Food::getMethodShort($model->method);?></div>
								<div><?= $model->age;?> ans</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="image-testimoni">
								<img src="<?= Url::base()?>/img/testimoni/<?= $model->image;?>" align="center">
							</div>
						</div>
						<div class="col-md-3">
						    <div class="font3-right-testimoni" align="center">
							    <div class="rcorners2" align="center">
						    		<div>
						    		<div>
							    	<span class="plus-font3-right-testimoni">
							    		+
							    	</span>
					    			<span class="number-font3-right-testimoni">
					    				50
					    			</span>
					    			<div class="div-font3-right-testimoni">
					    				<span class="span-div-font3-right-testimoni">
						    			<span class="span-font3-right-testimoni">kg</span>
						    			</span>
					    			</div>
					    			</div>
					    			</div>
					    			<div class="perdus-font3-right-testimoni">perdus</div>
							    </div>
						    </div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-12 div-line"></div>
					</div>
					<div class="col-md-12">
						<div class="text-testimoni" style="text-align: justify;">
						<?= $model->desc;?>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="col-md-12 div-line"></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>