<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Contact";
?>
<?php $this->registerCssFile("@web/css/page-contact.css")?>
<div class="content">
	<div class="row">
		<div class="col-md-12" id="div-pargh-contact">
			<div class="col-md-3"></div>
			<div class="col-md-6" id="main-content">
				<?php $form = ActiveForm::begin(['action'=>'page/contact'])?>
				<p><div id="title-main-content">Contact</div></p>
				<p>
					<div id="gender-main-content">
						<?= $form->field($contact, 'gender',['template'=>"Civilite* {input}\n{error}",'options'=>['class'=>'form-inline']])->radioList([
				                'mrs' => 'Madame', 
				                'mr' => 'Monsieur',
			            	],['class'=>'radio'])->label(false)?>
					</div>
				</p>
				<div class="col-md-6">
					<?= $form->field($contact, 'name')->textInput(['placeholder'=>'Nom*'])->label(false)?>
				</div>
				<div class="col-md-6 margin-bottom-20px">
					<?= $form->field($contact, 'firstname')->textInput(['placeholder'=>'Prenom*'])->label(false)?>
				</div>
				<div class="col-md-12 margin-bottom-20px">
					<?= $form->field($contact, 'email')->textInput(['placeholder'=>'Addresse mail*'])->label(false)?>
				</div>
				<div class="col-md-12 margin-bottom-20px">
					<?= $form->field($contact, 'body')->textArea(['placeholder'=>'Message*'])->label(false)?>
					*Champs obligatoires
				</div>
				<div class="col-md-2"></div>
				<div class="col-md-8" align="center">
					<?= Html::submitButton('ENVOYER',['id'=>'button-main-content'])?>
				</div>
				<div class="col-md-2"></div>
				<?php ActiveForm::end()?>

			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</div>
