<?php

use yii\db\Migration;

/**
 * Class m190909_071528_init_db
 */
class m190909_071528_init_db extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	$sql = "

-- -----------------------------------------------------
-- Table `account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `account` (
  `idaccount` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(250) NOT NULL,
  `role` INT(11) NOT NULL,
  `lastlogin` DATETIME NULL DEFAULT NULL,
  `status` INT(11) NOT NULL,
  PRIMARY KEY (`idaccount`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `coupon`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `coupon` (
  `idcoupon` INT(11) NOT NULL AUTO_INCREMENT,
  `idrefer` INT(11) NULL DEFAULT NULL,
  `coupontype` INT(11) NULL DEFAULT NULL,
  `createdate` DATETIME NULL DEFAULT NULL,
  `duedate` DATETIME NULL DEFAULT NULL,
  `code` VARCHAR(16) NULL DEFAULT NULL,
  `label` VARCHAR(45) NULL DEFAULT NULL,
  `day` INT(11) NULL DEFAULT NULL,
  `amount` DOUBLE NULL DEFAULT NULL,
  `useby` INT(11) NULL DEFAULT NULL,
  `usedate` DATETIME NULL DEFAULT NULL,
  `status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idcoupon`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `log` (
  `idlog` INT(11) NOT NULL AUTO_INCREMENT,
  `date` DATETIME NULL DEFAULT NULL,
  `content` TEXT NULL DEFAULT NULL,
  `idaccount` INT(11) NOT NULL,
  PRIMARY KEY (`idlog`),
  INDEX `fk_log_account1_idx` (`idaccount` ASC) ,
  CONSTRAINT `fk_log_account1`
    FOREIGN KEY (`idaccount`)
    REFERENCES `account` (`idaccount`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `payment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment` (
  `idpayment` INT(11) NOT NULL AUTO_INCREMENT,
  `idrefer` VARCHAR(45) NULL DEFAULT NULL,
  `code` VARCHAR(8) NULL DEFAULT NULL,
  `date` DATETIME NULL DEFAULT NULL,
  `day` INT(11) NULL DEFAULT NULL,
  `amount` DOUBLE NULL DEFAULT NULL,
  `status` INT(11) NULL DEFAULT NULL,
  `validby` VARCHAR(45) NULL DEFAULT NULL,
  `validdate` DATETIME NULL DEFAULT NULL,
  `log` TEXT NULL DEFAULT NULL,
  `idaccount` INT(11) NOT NULL,
  PRIMARY KEY (`idpayment`),
  INDEX `fk_payment_account1_idx` (`idaccount` ASC) ,
  CONSTRAINT `fk_payment_account1`
    FOREIGN KEY (`idaccount`)
    REFERENCES `account` (`idaccount`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `profile`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `profile` (
  `idprofile` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `fullname` VARCHAR(45) NULL DEFAULT NULL,
  `phone` VARCHAR(16) NULL DEFAULT NULL,
  `postalcode` VARCHAR(8) NULL DEFAULT NULL,
  `address` VARCHAR(250) NULL DEFAULT NULL,
  `city` VARCHAR(45) NULL DEFAULT NULL,
  `country` VARCHAR(45) NULL DEFAULT NULL,
  `gender` VARCHAR(1) NOT NULL,
  `dob` DATE NULL DEFAULT NULL,
  `motivation` VARCHAR(500) NULL DEFAULT NULL,
  `relationship` VARCHAR(25) NULL DEFAULT NULL,
  `children` INT(11) NULL DEFAULT NULL,
  `registerdate` DATETIME NULL DEFAULT NULL,
  `image` VARCHAR(150) NULL DEFAULT NULL,
  `idaccount` INT(11) NOT NULL,
  PRIMARY KEY (`idprofile`),
  INDEX `fk_profile_account1_idx` (`idaccount` ASC) ,
  CONSTRAINT `fk_profile_account1`
    FOREIGN KEY (`idaccount`)
    REFERENCES `account` (`idaccount`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `subscription`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `subscription` (
  `idsubscription` INT(11) NOT NULL AUTO_INCREMENT,
  `subscriptiontype` INT(11) NULL DEFAULT NULL,
  `createdate` DATETIME NULL DEFAULT NULL,
  `duedate` DATETIME NULL DEFAULT NULL,
  `status` INT(11) NULL DEFAULT NULL,
  `idaccount` INT(11) NOT NULL,
  `idpayment` INT(11) NOT NULL,
  PRIMARY KEY (`idsubscription`),
  INDEX `fk_subscription_account1_idx` (`idaccount` ASC) ,
  INDEX `fk_subscription_payment1_idx` (`idpayment` ASC) ,
  CONSTRAINT `fk_subscription_account1`
    FOREIGN KEY (`idaccount`)
    REFERENCES `account` (`idaccount`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_subscription_payment1`
    FOREIGN KEY (`idpayment`)
    REFERENCES `payment` (`idpayment`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `label`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `label` (
  `idlabel` INT NOT NULL,
  `key` VARCHAR(45) NULL,
  `val` TEXT NULL,
  `lang` VARCHAR(4) NULL,
  PRIMARY KEY (`idlabel`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `calc`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `calc` (
  `idcalc` INT(11) NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `system` INT NULL,
  `wactual` DOUBLE NULL,
  `wdesire` DOUBLE NULL,
  `wavg` DOUBLE NULL,
  `wmax` DOUBLE NULL,
  `wmin` DOUBLE NULL,
  `height` DOUBLE NULL,
  `dob` DATE NULL,
  `gene` INT NULL,
  `diet` INT NULL,
  `bone` INT NULL,
  `gender` VARCHAR(1) NULL,
  `pregnancy` INT NULL,
  `result` DOUBLE NULL,
  `score` INT NULL,
  `message` TEXT NULL,
  `act` INT NULL,
  `idaccount` INT(11) NOT NULL,
  PRIMARY KEY (`idcalc`),
  INDEX `fk_calc_account1_idx` (`idaccount` ASC) ,
  CONSTRAINT `fk_calc_account1`
    FOREIGN KEY (`idaccount`)
    REFERENCES `account` (`idaccount`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diet` (
  `iddiet` INT NOT NULL,
  `date` DATETIME NOT NULL,
  `method` INT NOT NULL,
  `phase` INT NOT NULL,
  `day` INT NOT NULL,
  `warn` INT NULL,
  `startdate` DATETIME NULL,
  `enddate` DATETIME NULL,
  `idaccount` INT(11) NOT NULL,
  `idsubscription` INT(11) NOT NULL,
  PRIMARY KEY (`iddiet`),
  INDEX `fk_diet_account1_idx` (`idaccount` ASC) ,
  INDEX `fk_diet_subscription1_idx` (`idsubscription` ASC) ,
  CONSTRAINT `fk_diet_account1`
    FOREIGN KEY (`idaccount`)
    REFERENCES `account` (`idaccount`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_diet_subscription1`
    FOREIGN KEY (`idsubscription`)
    REFERENCES `subscription` (`idsubscription`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `record`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `record` (
  `idrecord` INT NOT NULL,
  `type` INT NOT NULL,
  `day` INT NOT NULL,
  `dateinput` DATETIME NULL,
  `date` DATETIME NULL,
  `method` INT NOT NULL,
  `phase` INT NOT NULL,
  `format` INT NOT NULL,
  `color` VARCHAR(12) NULL,
  `wtarget` DOUBLE NOT NULL,
  `wreal` DOUBLE NULL,
  `starget` INT NOT NULL,
  `sreal` INT NULL,
  `pabdo` INT NULL,
  `pback` INT NULL,
  `pchest` INT NULL,
  `dtarget` INT NOT NULL,
  `dreal` INT NULL,
  `sontarget` DOUBLE NOT NULL,
  `sonreal` DOUBLE NULL,
  `motivation` INT NULL,
  `emotion` INT NULL,
  `fault` VARCHAR(45) NULL,
  `idaccount` INT(11) NOT NULL,
  `idcalc` INT NOT NULL,
  `iddiet` INT NOT NULL,
  PRIMARY KEY (`idrecord`),
  INDEX `fk_record_account2_idx` (`idaccount` ASC) ,
  INDEX `fk_record_calc1_idx` (`idcalc` ASC) ,
  INDEX `fk_record_diet1_idx` (`iddiet` ASC) ,
  CONSTRAINT `fk_record_account1`
    FOREIGN KEY (`idaccount`)
    REFERENCES `account` (`idaccount`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_record_calc1`
    FOREIGN KEY (`idcalc`)
    REFERENCES `calc` (`idcalc`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_record_diet1`
    FOREIGN KEY (`iddiet`)
    REFERENCES `diet` (`iddiet`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `foodtype`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `foodtype` (
  `idfoodtype` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idfoodtype`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `foodcat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `foodcat` (
  `idfoodcat` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idfoodcat`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `food`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `food` (
  `idfood` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `method` INT NULL,
  `phase` INT NULL,
  `image` VARCHAR(1024) NULL,
  `desc` TEXT NULL,
  `lang` VARCHAR(4) NULL,
  `country` VARCHAR(4) NULL,
  `idfoodtype` INT NOT NULL,
  `idfoodcat` INT NOT NULL,
  PRIMARY KEY (`idfood`),
  INDEX `fk_food_foodtype1_idx` (`idfoodtype` ASC) ,
  INDEX `fk_food_foodcat1_idx` (`idfoodcat` ASC) ,
  CONSTRAINT `fk_food_foodtype1`
    FOREIGN KEY (`idfoodtype`)
    REFERENCES `foodtype` (`idfoodtype`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_food_foodcat1`
    FOREIGN KEY (`idfoodcat`)
    REFERENCES `foodcat` (`idfoodcat`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `config` (
  `idconfig` INT NOT NULL,
  `key` VARCHAR(45) NULL,
  `val` TEXT NULL,
  `idaccount` INT(11) NOT NULL,
  PRIMARY KEY (`idconfig`),
  INDEX `fk_config_account1_idx` (`idaccount` ASC) ,
  CONSTRAINT `fk_config_account1`
    FOREIGN KEY (`idaccount`)
    REFERENCES `account` (`idaccount`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `device`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `device` (
  `iddevice` INT NOT NULL,
  `imei` VARCHAR(255) NOT NULL,
  `token` VARCHAR(1024) NOT NULL,
  `name` VARCHAR(255) NULL,
  `date` DATETIME NULL,
  `expire` DATETIME NULL,
  `idaccount` INT(11) NOT NULL,
  PRIMARY KEY (`iddevice`),
  UNIQUE INDEX `token_UNIQUE` (`token` ASC) ,
  UNIQUE INDEX `imei_UNIQUE` (`imei` ASC) ,
  INDEX `fk_device_account1_idx` (`idaccount` ASC) ,
  CONSTRAINT `fk_device_account1`
    FOREIGN KEY (`idaccount`)
    REFERENCES `account` (`idaccount`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
";

	return $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190909_071528_init_db cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190909_071528_init_db cannot be reverted.\n";

        return false;
    }
    */
}
